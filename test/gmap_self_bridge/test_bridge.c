/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "test_bridge.h"
#include "gmap_self_bridge.h"
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include "../common/mock_netmodel.h"
#include "../common/mock_gmap.h"

#include "gmap_self_disco.h"
#include "gmap_self_util.h"

#include <debug/sahtrace.h>
#include "../common/test-setup.h"
#include <amxd/amxd_object_parameter.h>
#include <amxc/amxc_macros.h>
#include <netmodel/client.h>
#include <amxs/amxs.h>
#include <sys/socket.h>

void test_gmap_self_bridge_new__normal_case(UNUSED void** state) {
    bool ok = false;
    gmap_self_selfdev_t* selfdev = NULL;
    gmap_self_bridge_t* bridge = NULL;
    mock_netmodel_query_t mac_query = {.key = "somedev", .value = "12:34:56:ab:cd:ef" };
    mock_netmodel_query_t ip_query = {.init_cb_data_json = "netmodel-mockdata-empty-addresses.json"};

    // GIVEN a selfdev and a bridge in netmodel
    // selfdev:
    selfdev = test_setup_create_selfdev();
    // bridge in netmodel:
    test_setup_parse_odl("../common/data/netmodel-data.odl");
    mock_netmodel_expect_openQuery_getParameters("bridge-lan_bridge", "MACAddress", NULL, "up", &mac_query);
    mock_netmodel_expect_openQuery_getAddrs("bridge-lan_bridge", NULL, "up", &ip_query);

    // EXPECT the device in gmap to be created, with correct name, flags
    mock_gmap_ip_device_get_addresses("bridge-lan_bridge", "gmap-mockdata-empty-addresses.json");
    mock_gmap_expect_createDevice("bridge-lan_bridge", "gmap-self", "self lan protected mac interface bridge ipv4 ipv6", true, NULL, true);

    // EXPECT "Active" field of device to be set
    mock_gmap_expect_setActive("bridge-lan_bridge", true, true);

    // EXPECT selfdev to be parent of bridge
    mock_gmap_expect_linkAdd("superDuperHGW", "bridge-lan_bridge", NULL, "mod-self", true);


    // WHEN creating a new bridge
    ok = gmap_self_bridge_new(&bridge, selfdev, "bridge-lan_bridge");

    // THEN the MAC and other parameters are synced
    assert_true(ok);
    assert_non_null(bridge);
    mock_gmap_assert_device_param_string("bridge-lan_bridge", "PhysAddress", "12:34:56:ab:cd:ef");
    mock_gmap_assert_device_param_uint32("bridge-lan_bridge", "NetDevIndex", 18);
    mock_gmap_assert_device_param_string("bridge-lan_bridge", "NetDevName", "br-lan");

    gmap_self_bridge_delete(&bridge, true);
    gmap_self_selfdev_delete(&selfdev);
}

void test_gmap_self_bridge_new__mac_is_late(UNUSED void** state) {
    bool ok = false;
    gmap_self_selfdev_t* selfdev = NULL;
    gmap_self_bridge_t* bridge = NULL;
    mock_netmodel_query_t mac_query = {0};
    mock_netmodel_query_t ip_query = {.init_cb_data_json = "netmodel-mockdata-empty-addresses.json"};

    // GIVEN a bridge that does not know its mac address yet
    selfdev = test_setup_create_selfdev();
    test_setup_parse_odl("../common/data/netmodel-data.odl");
    mock_netmodel_expect_openQuery_getParameters("bridge-lan_bridge", "MACAddress", NULL, "up", &mac_query);
    mock_netmodel_expect_openQuery_getAddrs("bridge-lan_bridge", NULL, "up", &ip_query);
    mock_gmap_ip_device_get_addresses("bridge-lan_bridge", "gmap-mockdata-empty-addresses.json");
    mock_gmap_expect_createDevice("bridge-lan_bridge", "gmap-self", "self lan protected mac interface bridge ipv4 ipv6", true, NULL, true);
    mock_gmap_expect_setActive("bridge-lan_bridge", true, true);
    mock_gmap_expect_linkAdd("superDuperHGW", "bridge-lan_bridge", NULL, "mod-self", true);
    ok = gmap_self_bridge_new(&bridge, selfdev, "bridge-lan_bridge");
    assert_true(ok);
    assert_non_null(bridge);

    // WHEN netmodel knows the bridge's mac
    mock_netmodel_call_getParameters_callback(&mac_query, "themacdev", "66:55:44:33:22:11");

    // THEN the MAC is synced to gmap
    mock_gmap_assert_device_param_string("bridge-lan_bridge", "PhysAddress", "66:55:44:33:22:11");

    gmap_self_bridge_delete(&bridge, true);
    gmap_self_selfdev_delete(&selfdev);
}

void test_gmap_self_bridge__mac_changes(UNUSED void** state) {
    bool ok = false;
    gmap_self_selfdev_t* selfdev = NULL;
    gmap_self_bridge_t* bridge = NULL;
    mock_netmodel_query_t mac_query = {.key = "somedev", .value = "12:34:56:ab:cd:ef" };
    mock_netmodel_query_t ip_query = {.init_cb_data_json = "netmodel-mockdata-empty-addresses.json"};

    // GIVEN a bridge with a mac
    selfdev = test_setup_create_selfdev();
    test_setup_parse_odl("../common/data/netmodel-data.odl");
    mock_netmodel_expect_openQuery_getParameters("bridge-lan_bridge", "MACAddress", NULL, "up", &mac_query);
    mock_netmodel_expect_openQuery_getAddrs("bridge-lan_bridge", NULL, "up", &ip_query);
    mock_gmap_ip_device_get_addresses("bridge-lan_bridge", "gmap-mockdata-empty-addresses.json");
    mock_gmap_expect_createDevice("bridge-lan_bridge", "gmap-self", "self lan protected mac interface bridge ipv4 ipv6", true, NULL, true);
    mock_gmap_expect_setActive("bridge-lan_bridge", true, true);
    mock_gmap_expect_linkAdd("superDuperHGW", "bridge-lan_bridge", NULL, "mod-self", true);
    ok = gmap_self_bridge_new(&bridge, selfdev, "bridge-lan_bridge");
    assert_true(ok);
    assert_non_null(bridge);
    mock_gmap_assert_device_param_string("bridge-lan_bridge", "PhysAddress", "12:34:56:ab:cd:ef");

    // WHEN netmodel mac changes
    mock_netmodel_call_getParameters_callback(&mac_query, "themacdev", "66:55:44:33:22:11");

    // THEN the MAC in gmap changes as well
    mock_gmap_assert_device_param_string("bridge-lan_bridge", "PhysAddress", "66:55:44:33:22:11");

    // WHEN netmodel mac changes again
    mock_netmodel_call_getParameters_callback(&mac_query, "otherDev", "66:55:44:33:22:00");

    // THEN the MAC in gmap changes again as well
    mock_gmap_assert_device_param_string("bridge-lan_bridge", "PhysAddress", "66:55:44:33:22:00");

    gmap_self_bridge_delete(&bridge, true);
    gmap_self_selfdev_delete(&selfdev);
}

void test_gmap_self_bridge_new__device_creation_failure(UNUSED void** state) {
    bool ok = true;
    gmap_self_selfdev_t* selfdev = NULL;
    gmap_self_bridge_t* bridge = NULL;

    // GIVEN a selfdev and a bridge in netmodel
    // selfdev:
    selfdev = test_setup_create_selfdev();
    // bridge in netmodel:
    test_setup_parse_odl("../common/data/netmodel-data.odl");

    // EXPECT the device in gmap creation will fail
    mock_gmap_expect_createDevice("bridge-lan_bridge", "gmap-self", "self lan protected mac interface bridge ipv4 ipv6", true, NULL, false);

    // WHEN creating a new bridge
    ok = gmap_self_bridge_new(&bridge, selfdev, "bridge-lan_bridge");

    // THEN it is reported as failed
    assert_false(ok);
    assert_null(bridge);

    gmap_self_selfdev_delete(&selfdev);
}

void test_gmap_self_bridge_new__bridge_has_ip(UNUSED void** state) {
    bool ok = false;
    gmap_self_selfdev_t* selfdev = NULL;
    gmap_self_bridge_t* bridge = NULL;
    mock_netmodel_query_t mac_query = {};
    mock_netmodel_query_t ip_query = {.init_cb_data_json = "netmodel-mockdata-normal.json"};

    // GIVEN gmap, a selfdev and a bridge in netmodel that has an IP
    // selfdev:
    selfdev = test_setup_create_selfdev();
    // bridge in netmodel:
    test_setup_parse_odl("../common/data/netmodel-data.odl");
    mock_netmodel_expect_openQuery_getParameters("bridge-lan_bridge", "MACAddress", NULL, "up", &mac_query);

    // EXPECT the bridge to be given an IP in gmap
    mock_gmap_ip_device_get_addresses("bridge-lan_bridge", "gmap-mockdata-empty-addresses.json");
    mock_gmap_expect_ip_device_add_address("bridge-lan_bridge", AF_INET, "192.168.1.1", "global", "reachable", "self", true, amxd_status_ok);
    mock_gmap_expect_ip_device_add_address("bridge-lan_bridge", AF_INET6, "fe80::211:11ff:fe01:2401", "link", "reachable", "self", true, amxd_status_ok);
    // (unrelated things that also happen):
    mock_gmap_expect_createDevice("bridge-lan_bridge", "gmap-self", "self lan protected mac interface bridge ipv4 ipv6", true, NULL, true);
    mock_gmap_expect_setActive("bridge-lan_bridge", true, true);
    mock_gmap_expect_linkAdd("superDuperHGW", "bridge-lan_bridge", NULL, "mod-self", true);

    // WHEN creating a new bridge
    mock_netmodel_expect_openQuery_getAddrs("bridge-lan_bridge", NULL, "up", &ip_query);
    ok = gmap_self_bridge_new(&bridge, selfdev, "bridge-lan_bridge");
    assert_true(ok);
    assert_non_null(bridge);

    gmap_self_bridge_delete(&bridge, true);
    gmap_self_selfdev_delete(&selfdev);
}

static void s_new_bridge(gmap_self_bridge_t** bridge, gmap_self_selfdev_t** selfdev, mock_netmodel_query_t* ip_query, mock_netmodel_query_t* mac_query) {
    bool ok = false;
    *mac_query = (mock_netmodel_query_t) {.key = "somedev", .value = "12:34:56:ab:cd:ef" };
    *ip_query = (mock_netmodel_query_t) {.init_cb_data_json = "netmodel-mockdata-empty-addresses.json"};

    // selfdev:
    *selfdev = test_setup_create_selfdev();
    // bridge in netmodel:
    test_setup_parse_odl("../common/data/netmodel-data.odl");
    mock_netmodel_expect_openQuery_getParameters("bridge-lan_bridge", "MACAddress", NULL, "up", mac_query);
    mock_netmodel_expect_openQuery_getAddrs("bridge-lan_bridge", NULL, "up", ip_query);

    // device created:
    mock_gmap_ip_device_get_addresses("bridge-lan_bridge", "gmap-mockdata-empty-addresses.json");
    mock_gmap_expect_createDevice("bridge-lan_bridge", "gmap-self", "self lan protected mac interface bridge ipv4 ipv6", true, NULL, true);

    // active set:
    mock_gmap_expect_setActive("bridge-lan_bridge", true, true);

    // link:
    mock_gmap_expect_linkAdd("superDuperHGW", "bridge-lan_bridge", NULL, "mod-self", true);

    // creating bridge
    ok = gmap_self_bridge_new(bridge, *selfdev, "bridge-lan_bridge");
    assert_true(ok);
}

void test_gmap_self_bridge__ip_appears_late(UNUSED void** state) {
    mock_netmodel_query_t ip_query = {0};
    mock_netmodel_query_t mac_query = {0};
    gmap_self_bridge_t* bridge = NULL;
    gmap_self_selfdev_t* selfdev = NULL;

    // GIVEN a gmap bridge
    s_new_bridge(&bridge, &selfdev, &ip_query, &mac_query);

    // EXPECT address to be added:
    mock_gmap_ip_device_get_addresses("bridge-lan_bridge", "gmap-mockdata-empty-addresses.json");
    mock_gmap_expect_ip_device_add_address("bridge-lan_bridge", AF_INET, "192.168.1.1", "global", "reachable", "self", true, amxd_status_ok);
    mock_gmap_expect_ip_device_add_address("bridge-lan_bridge", AF_INET6, "fe80::211:11ff:fe01:2401", "link", "reachable", "self", true, amxd_status_ok);

    // WHEN netmodel reports the bridge has a new IP
    mock_netmodel_call_getAddrs_callback(&ip_query, "netmodel-mockdata-normal.json");

    gmap_self_bridge_delete(&bridge, true);
    gmap_self_selfdev_delete(&selfdev);
}

void test_gmap_self_bridge__ip_disappears(UNUSED void** state) {
    mock_netmodel_query_t ip_query = {};
    mock_netmodel_query_t mac_query = {};
    gmap_self_bridge_t* bridge = NULL;
    gmap_self_selfdev_t* selfdev = NULL;

    // GIVEN a gmap bridge with two IPs
    s_new_bridge(&bridge, &selfdev, &ip_query, &mac_query);
    mock_gmap_ip_device_get_addresses("bridge-lan_bridge", "gmap-mockdata-empty-addresses.json");
    mock_gmap_expect_ip_device_add_address("bridge-lan_bridge", AF_INET, "192.168.1.1", "global", "reachable", "self", true, amxd_status_ok);
    mock_gmap_expect_ip_device_add_address("bridge-lan_bridge", AF_INET6, "fe80::211:11ff:fe01:2401", "link", "reachable", "self", true, amxd_status_ok);
    mock_netmodel_call_getAddrs_callback(&ip_query, "netmodel-mockdata-normal.json");

    // EXPECT one IP to be removed
    mock_gmap_ip_device_get_addresses("bridge-lan_bridge", "gmap-mockdata-normal.json");
    mock_gmap_ip_device_delete_address("bridge-lan_bridge", AF_INET, "192.168.1.1");

    // WHEN netmodel reports that one IP is gone
    mock_netmodel_call_getAddrs_callback(&ip_query, "netmodel-mockdata-only-ipv6.json");

    gmap_self_bridge_delete(&bridge, true);
    gmap_self_selfdev_delete(&selfdev);
}

void test_gmap_self_bridge__ip_changes(UNUSED void** state) {
    mock_netmodel_query_t ip_query = {};
    mock_netmodel_query_t mac_query = {};
    gmap_self_bridge_t* bridge = NULL;
    gmap_self_selfdev_t* selfdev = NULL;

    // GIVEN a gmap bridge with two IPs
    s_new_bridge(&bridge, &selfdev, &ip_query, &mac_query);
    mock_gmap_ip_device_get_addresses("bridge-lan_bridge", "gmap-mockdata-empty-addresses.json");
    mock_gmap_expect_ip_device_add_address("bridge-lan_bridge", AF_INET, "192.168.1.1", "global", "reachable", "self", true, amxd_status_ok);
    mock_gmap_expect_ip_device_add_address("bridge-lan_bridge", AF_INET6, "fe80::211:11ff:fe01:2401", "link", "reachable", "self", true, amxd_status_ok);
    mock_netmodel_call_getAddrs_callback(&ip_query, "netmodel-mockdata-normal.json");

    // EXPECT one IP to be removed and one to be added
    mock_gmap_ip_device_get_addresses("bridge-lan_bridge", "gmap-mockdata-normal.json");
    mock_gmap_ip_device_delete_address("bridge-lan_bridge", AF_INET, "192.168.1.1");
    mock_gmap_expect_ip_device_add_address("bridge-lan_bridge", AF_INET, "192.168.1.2", "global", "reachable", "self", true, amxd_status_ok);

    // WHEN netmodel reports that one IP has changed
    mock_netmodel_call_getAddrs_callback(&ip_query, "netmodel-mockdata-normal-ip2.json");

    gmap_self_bridge_delete(&bridge, true);
    gmap_self_selfdev_delete(&selfdev);
}