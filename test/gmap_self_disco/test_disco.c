/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "test_disco.h"
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include "../common/mock_gmap_self_bridge.h"
#include "../common/mock_gmap_self_eth.h"
#include "../common/mock_netmodel.h"
#include "../common/mock_gmap.h"

#include "gmap_self_disco.h"
#include "gmap_self_util.h"

#include <debug/sahtrace.h>
#include "../common/test-setup.h"
#include <amxd/amxd_object_parameter.h>
#include <amxc/amxc_macros.h>
#include <netmodel/client.h>
#include <amxs/amxs.h>

void test_gmap_self_disco_appear_bridge(UNUSED void** state) {
    bool ok = false;
    gmap_self_disco_t* disco = NULL;
    gmap_self_selfdev_t* selfdev = test_setup_create_selfdev();
    mock_netmodel_query_t query_lo = {0};
    mock_netmodel_query_t query_br1 = {0};
    mock_netmodel_query_t query_br2 = {0};
    mock_gmap_self_bridge_t bridge1 = {.bridge.name = (char*) "bridge1", .selfdev = selfdev };
    mock_gmap_self_bridge_t bridge2 = {.bridge.name = (char*) "bridge2", .selfdev = selfdev };

    // GIVEN a running discovery
    mock_netmodel_expect_openQuery_getIntfs("resolver", "bridge", "all", &query_lo);
    ok = gmap_self_disco_new(&disco, selfdev);
    ok &= gmap_self_disco_start(disco);
    assert_true(ok);
    handle_events();

    // EXPECT a new bridge to be created
    mock_gmap_self_bridge_expect_new(&bridge1);
    mock_netmodel_expect_openQuery_getIntfs("bridge1", "eth_intf || ssid || wds || moca", "down", &query_br1);

    // WHEN new bridges appears
    mock_netmodel_call_getintf_callback(&query_lo, "bridge1", NULL, NULL);

    // EXPECT another new bridge to be created
    mock_gmap_self_bridge_expect_new(&bridge2);
    mock_netmodel_expect_openQuery_getIntfs("bridge2", "eth_intf || ssid || wds || moca", "down", &query_br2);

    // WHEN more bridges appears
    mock_netmodel_call_getintf_callback(&query_lo, "bridge1", "bridge2", NULL);

    gmap_self_disco_delete(&disco);
    gmap_self_selfdev_delete(&selfdev);
}

void test_gmap_self_disco_appear_disallowed_bridge(UNUSED void** state) {
    bool ok = false;
    gmap_self_disco_t* disco = NULL;
    // Note: this sets allowed bridges to "bridge1,bridge2,bridge3,my_bridge"
    gmap_self_selfdev_t* selfdev = test_setup_create_selfdev();
    mock_netmodel_query_t query_lo = {0};

    // GIVEN a running discovery
    mock_netmodel_expect_openQuery_getIntfs("resolver", "bridge", "all", &query_lo);
    ok = gmap_self_disco_new(&disco, selfdev);
    ok &= gmap_self_disco_start(disco);
    assert_true(ok);
    handle_events();

    // EXPECT no new bridge to be created

    // WHEN a bridges appears that is not allowed in Devices.Config.mod-self.Bridges
    mock_netmodel_call_getintf_callback(&query_lo, "notallowedbridge", NULL, NULL);

    gmap_self_disco_delete(&disco);
    gmap_self_selfdev_delete(&selfdev);
}

void test_gmap_self_disco_bridge_from_start(UNUSED void** state) {
    bool ok = false;
    gmap_self_disco_t* disco = NULL;
    gmap_self_selfdev_t* selfdev = test_setup_create_selfdev();
    mock_netmodel_query_t query_br1 = {0};
    mock_gmap_self_bridge_t bridge1 = {.bridge.name = (char*) "bridge1", .selfdev = selfdev };

    // GIVEN a disco, and a netmodel query that will trigger callback
    //       immediately upon query creation and not after eventloop was re-entered.
    mock_netmodel_query_t query_lo = {.intf1 = "notallowedbridge1", .intf2 = "bridge1", .intf3 = "notallowedbridge2"};
    ok = gmap_self_disco_new(&disco, selfdev);
    assert_true(ok);

    // EXPECT the netmodel query to be opened and a new bridge to be created for "bridge1".
    mock_netmodel_expect_openQuery_getIntfs("resolver", "bridge", "all", &query_lo);
    mock_gmap_self_bridge_expect_new(&bridge1);
    mock_netmodel_expect_openQuery_getIntfs("bridge1", "eth_intf || ssid || wds || moca", "down", &query_br1);

    // EXPECT nothing to happen for "notallowedbridge1" and "notallowedbridge2"
    //        because they are not allowed according to Devices.Config.mod-self.Bridges

    // WHEN starting disco
    ok = gmap_self_disco_start(disco);
    assert_true(ok);
    handle_events();

    gmap_self_disco_delete(&disco);
    gmap_self_selfdev_delete(&selfdev);
}

void test_gmap_self_disco_appear_bridge_fail_create(UNUSED void** state) {
    bool ok = false;
    gmap_self_disco_t* disco = NULL;
    gmap_self_selfdev_t* selfdev = test_setup_create_selfdev();
    mock_netmodel_query_t query_lo = {0};
    mock_netmodel_query_t query_br1 = {0};
    mock_gmap_self_bridge_t bridge1 = {.bridge.name = (char*) "bridge1", .selfdev = selfdev};

    // GIVEN a running discovery
    mock_netmodel_expect_openQuery_getIntfs("resolver", "bridge", "all", &query_lo);
    ok = gmap_self_disco_new(&disco, selfdev);
    ok &= gmap_self_disco_start(disco);
    assert_true(ok);
    handle_events();

    // EXPECT it will fail to create a new bridge
    bridge1.fail_on_create = true;
    mock_gmap_self_bridge_expect_new(&bridge1);
    // and expect close query (not tracked)

    // WHEN a new bridge appears
    mock_netmodel_call_getintf_callback(&query_lo, "bridge1", NULL, NULL);

    // EXPECT bridge will be attempted to create again
    bridge1.fail_on_create = false;
    mock_gmap_self_bridge_expect_new(&bridge1);
    mock_netmodel_expect_openQuery_getIntfs("bridge1", "eth_intf || ssid || wds || moca", "down", &query_br1);

    // WHEN the bridge is still present according to NetModel
    mock_netmodel_call_getintf_callback(&query_lo, "bridge1", NULL, NULL);

    gmap_self_disco_delete(&disco);
    gmap_self_selfdev_delete(&selfdev);
}

typedef struct {
    mock_gmap_self_bridge_t br1;
    mock_gmap_self_bridge_t br2;
    mock_gmap_self_bridge_t br3;
    mock_netmodel_query_t query_lo;
    mock_netmodel_query_t query_br1;
    mock_netmodel_query_t query_br2;
    mock_netmodel_query_t query_br3;
} some_bridges_t;

static void s_create_some_bridges(gmap_self_selfdev_t* selfdev, some_bridges_t* queries) {
    queries->br1.selfdev = selfdev;
    queries->br2.selfdev = selfdev;
    queries->br3.selfdev = selfdev;
    queries->br1.bridge.name = (char*) "bridge1";
    queries->br2.bridge.name = (char*) "bridge2";
    queries->br3.bridge.name = (char*) "bridge3";
    mock_netmodel_expect_openQuery_getIntfs("bridge1", "eth_intf || ssid || wds || moca", "down", &queries->query_br1);
    mock_gmap_self_bridge_expect_new(&queries->br1);
    mock_netmodel_expect_openQuery_getIntfs("bridge2", "eth_intf || ssid || wds || moca", "down", &queries->query_br2);
    mock_gmap_self_bridge_expect_new(&queries->br2);
    mock_netmodel_expect_openQuery_getIntfs("bridge3", "eth_intf || ssid || wds || moca", "down", &queries->query_br3);
    mock_gmap_self_bridge_expect_new(&queries->br3);
    mock_netmodel_call_getintf_callback(&queries->query_lo, "bridge1", "bridge2", "bridge3");
    handle_events();
}

void test_gmap_self_disco_disappear_bridge(UNUSED void** state) {
    bool ok = false;
    gmap_self_disco_t* disco = NULL;
    gmap_self_selfdev_t* selfdev = test_setup_create_selfdev();
    some_bridges_t bridges = {0};

    // GIVEN discovery with some bridges
    mock_netmodel_expect_openQuery_getIntfs("resolver", "bridge", "all", &bridges.query_lo);
    ok = gmap_self_disco_new(&disco, selfdev);
    ok &= gmap_self_disco_start(disco);
    assert_true(ok);
    s_create_some_bridges(selfdev, &bridges);

    // WHEN a bridge disappears
    mock_netmodel_call_getintf_callback(&bridges.query_lo, "bridge1", "bridge3", NULL);

    // THEN that bridge is deleted
    // (and query on bridge is closed, but we currently don't track this)
    assert_int_equal(0, bridges.br1.deletion_count);
    assert_int_equal(1, bridges.br2.deletion_count);
    assert_int_equal(0, bridges.br3.deletion_count);

    gmap_self_disco_delete(&disco);
    gmap_self_selfdev_delete(&selfdev);
}

void test_gmap_self_disco_appear_eth(UNUSED void** state) {
    bool ok = false;
    gmap_self_disco_t* disco = NULL;
    gmap_self_selfdev_t* selfdev = test_setup_create_selfdev();
    some_bridges_t bridges = {0};
    mock_gmap_self_eth_t my_port = {.eth.name = (char*) "my_port", .eth.bridge = &bridges.br2.bridge };

    // GIVEN discovery with some bridges
    mock_netmodel_expect_openQuery_getIntfs("resolver", "bridge", "all", &bridges.query_lo);
    ok = gmap_self_disco_new(&disco, selfdev);
    ok &= gmap_self_disco_start(disco);
    assert_true(ok);
    s_create_some_bridges(selfdev, &bridges);

    // EXPECT an eth port to be created in gmap
    mock_gmap_self_eth_expect_new(&my_port);

    // WHEN an eth port appears in netmodel
    mock_netmodel_call_getintf_callback(&bridges.query_br2, "my_port", NULL, NULL);

    gmap_self_disco_delete(&disco);
    gmap_self_selfdev_delete(&selfdev);
}

void test_gmap_self_disco_appear_vap(UNUSED void** state) {
    bool ok = false;
    gmap_self_disco_t* disco = NULL;
    gmap_self_selfdev_t* selfdev = test_setup_create_selfdev();
    some_bridges_t bridges = {0};
    mock_gmap_self_eth_t my_port = {.eth.name = (char*) "ssid-vap2g0priv", .eth.bridge = &bridges.br2.bridge, .is_wifi = true};

    // GIVEN discovery with some bridges
    mock_netmodel_expect_openQuery_getIntfs("resolver", "bridge", "all", &bridges.query_lo);
    ok = gmap_self_disco_new(&disco, selfdev);
    ok &= gmap_self_disco_start(disco);
    assert_true(ok);
    s_create_some_bridges(selfdev, &bridges);

    // EXPECT an eth port to be created in gmap
    mock_gmap_self_eth_expect_new(&my_port);

    // WHEN an eth port appears in netmodel
    mock_netmodel_call_getintf_callback(&bridges.query_br2, "ssid-vap2g0priv", NULL, NULL);

    gmap_self_disco_delete(&disco);
    gmap_self_selfdev_delete(&selfdev);
}

void test_gmap_self_disco_eth_from_start(UNUSED void** state) {
    bool ok = false;
    gmap_self_disco_t* disco = NULL;
    gmap_self_selfdev_t* selfdev = test_setup_create_selfdev();
    mock_gmap_self_bridge_t mock_bridge = {.bridge.name = (char*) "my_bridge", .selfdev = selfdev };
    mock_gmap_self_eth_t mock_eth = {.eth.name = (char*) "my_port", .eth.bridge = &mock_bridge.bridge };

    // GIVEN a search-ethernet-ports and a search-bridge netmodel query that upon opening
    //       immediately calls its callback
    //       instead of calling callback from eventloop
    mock_netmodel_query_t query_lo = { .intf1 = "my_bridge" };
    mock_netmodel_query_t query_br1 = { .intf1 = "my_port" };
    mock_netmodel_expect_openQuery_getIntfs("resolver", "bridge", "all", &query_lo);
    mock_netmodel_expect_openQuery_getIntfs("my_bridge", "eth_intf || ssid || wds || moca", "down", &query_br1);

    // EXPECT both the bridge and the eth port to be created in gmap
    mock_gmap_self_bridge_expect_new(&mock_bridge);
    mock_gmap_self_eth_expect_new(&mock_eth);

    // WHEN starting disco
    ok = gmap_self_disco_new(&disco, selfdev);
    ok &= gmap_self_disco_start(disco);
    assert_true(ok);
    handle_events();

    gmap_self_disco_delete(&disco);
    gmap_self_selfdev_delete(&selfdev);
}

void test_gmap_self_disco_move_eth_first_remove_received(UNUSED void** state) {
    bool ok = false;
    gmap_self_disco_t* disco = NULL;
    gmap_self_selfdev_t* selfdev = test_setup_create_selfdev();
    some_bridges_t bridges = {0};
    mock_gmap_self_eth_t fixed_port = {.eth.name = (char*) "fixed_port", .eth.bridge = &bridges.br1.bridge };
    mock_gmap_self_eth_t movable_port = {.eth.name = (char*) "movable_port", .eth.bridge = &bridges.br1.bridge };

    // GIVEN bridge discovery with some eth ports
    mock_netmodel_expect_openQuery_getIntfs("resolver", "bridge", "all", &bridges.query_lo);
    ok = gmap_self_disco_new(&disco, selfdev);
    ok &= gmap_self_disco_start(disco);
    assert_true(ok);
    s_create_some_bridges(selfdev, &bridges);
    mock_gmap_self_eth_expect_new(&fixed_port);
    mock_gmap_self_eth_expect_new(&movable_port);
    mock_netmodel_call_getintf_callback(&bridges.query_br1, "fixed_port", "movable_port", NULL);

    // EXPECT the eth port to be unlinked (not being of any bridge)
    mock_gmap_self_eth_expect_set_bridge(&movable_port, NULL);

    // WHEN an eth port moves from bridge1 to bridge2
    //      by receiving first the info that it is not present anymore in bridge1
    mock_netmodel_call_getintf_callback(&bridges.query_br1, "fixed_port", NULL, NULL);

    // THEN the eth dev is not deleted
    assert_int_equal(0, movable_port.deletion_count);

    // EXPECT the eth port is linked to the new bridge
    mock_gmap_self_eth_expect_set_bridge(&movable_port, &bridges.br2);

    // WHEN the eth port moving completes by receiving the info that it is present in bridge2
    mock_netmodel_call_getintf_callback(&bridges.query_br2, "movable_port", NULL, NULL);

    gmap_self_disco_delete(&disco);
    gmap_self_selfdev_delete(&selfdev);
}

void test_gmap_self_disco_move_eth_first_add_received(UNUSED void** state) {
    bool ok = false;
    gmap_self_disco_t* disco = NULL;
    gmap_self_selfdev_t* selfdev = test_setup_create_selfdev();
    some_bridges_t bridges = {0};
    mock_gmap_self_eth_t fixed_port = {.eth.name = (char*) "fixed_port", .eth.bridge = &bridges.br1.bridge };
    mock_gmap_self_eth_t movable_port = {.eth.name = (char*) "movable_port", .eth.bridge = &bridges.br1.bridge };

    // GIVEN bridge discovery with some eth ports
    mock_netmodel_expect_openQuery_getIntfs("resolver", "bridge", "all", &bridges.query_lo);
    ok = gmap_self_disco_new(&disco, selfdev);
    ok &= gmap_self_disco_start(disco);
    assert_true(ok);
    s_create_some_bridges(selfdev, &bridges);
    mock_gmap_self_eth_expect_new(&fixed_port);
    mock_gmap_self_eth_expect_new(&movable_port);
    mock_netmodel_call_getintf_callback(&bridges.query_br1, "fixed_port", "movable_port", NULL);

    // EXPECT the eth port is linked to the new bridge (replacing the old link)
    mock_gmap_self_eth_expect_set_bridge(&movable_port, &bridges.br2);

    // WHEN the eth port move by first receiving the info that it is present in bridge2
    //      before receiving that it is removed from bridge1
    mock_netmodel_call_getintf_callback(&bridges.query_br2, "movable_port", NULL, NULL);
    mock_netmodel_call_getintf_callback(&bridges.query_br1, "fixed_port", NULL, NULL);

    // THEN the eth dev was never deleted
    assert_int_equal(0, movable_port.deletion_count);
    assert_int_equal(0, fixed_port.deletion_count);

    gmap_self_disco_delete(&disco);
    gmap_self_selfdev_delete(&selfdev);
}

void test_gmap_self_disco_remove_eth(UNUSED void** state) {
    bool ok = false;
    gmap_self_disco_t* disco = NULL;
    gmap_self_selfdev_t* selfdev = test_setup_create_selfdev();
    some_bridges_t bridges = {0};
    mock_gmap_self_eth_t ethport = {.eth.name = (char*) "port", .eth.bridge = &bridges.br1.bridge };

    // GIVEN bridge discovery with an eth port
    mock_netmodel_expect_openQuery_getIntfs("resolver", "bridge", "all", &bridges.query_lo);
    ok = gmap_self_disco_new(&disco, selfdev);
    ok &= gmap_self_disco_start(disco);
    assert_true(ok);
    s_create_some_bridges(selfdev, &bridges);
    mock_gmap_self_eth_expect_new(&ethport);
    mock_netmodel_call_getintf_callback(&bridges.query_br1, "port", NULL, NULL);

    // EXPECT the eth port is unlinked
    mock_gmap_self_eth_expect_set_bridge(&ethport, NULL);

    // WHEN the eth port is gone in netmodel (e.g. usb unplug)
    mock_netmodel_call_getintf_callback(&bridges.query_br1, NULL, NULL, NULL);

    // THEN the eth port was never deleted
    assert_int_equal(0, ethport.deletion_count);

    gmap_self_disco_delete(&disco);
    assert_int_equal(1, ethport.deletion_count);
    gmap_self_selfdev_delete(&selfdev);
}

void test_gmap_self_disco_brige_of_eth_disappears(UNUSED void** state) {
    // GIVEN bridge discovery with an eth port
    bool ok = false;
    some_bridges_t bridges = {0};
    mock_gmap_self_eth_t ethport = {.eth.name = (char*) "port", .eth.bridge = &bridges.br1.bridge };
    gmap_self_selfdev_t* selfdev = test_setup_create_selfdev();
    gmap_self_disco_t* disco = NULL;
    mock_netmodel_expect_openQuery_getIntfs("resolver", "bridge", "all", &bridges.query_lo);
    ok = gmap_self_disco_new(&disco, selfdev);
    ok &= gmap_self_disco_start(disco);
    assert_true(ok);
    s_create_some_bridges(selfdev, &bridges);
    mock_gmap_self_eth_expect_new(&ethport);
    mock_netmodel_call_getintf_callback(&bridges.query_br1, "port", NULL, NULL);

    // EXPECT the eth port to not be linked to its bridge anymore
    mock_gmap_self_eth_expect_set_bridge(&ethport, NULL);

    // WHEN the bridge disappears
    mock_netmodel_call_getintf_callback(&bridges.query_lo, "bridge2", "bridge3", NULL);

    // THEN the eth port is not deleted
    assert_int_equal(0, bridges.br3.deletion_count);

    gmap_self_disco_delete(&disco);
    gmap_self_selfdev_delete(&selfdev);
}
