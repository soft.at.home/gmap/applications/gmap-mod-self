/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef __COMMON_MOCK_NETMODEL_H__
#define __COMMON_MOCK_NETMODEL_H__


#include <netmodel/common_api.h>
#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxo/amxo.h>
#include <amxb/amxb_register.h>
#include <amxb/amxb_be.h>

/** transparent type */
typedef struct {
    void* userdata;
    netmodel_callback_t callback;
    char* intfname;
    /** Initial query result for queries that give lists */
    const char* intf1;
    const char* intf2;
    const char* intf3;
    /** Initial query result for queries that give hashtables */
    const char* key;
    const char* value;
    /** Initial query result for queries that get their data from a json file */
    const char* init_cb_data_json;
} mock_netmodel_query_t;

amxb_bus_ctx_t* __wrap_netmodel_get_amxb_bus(void);
bool __wrap_netmodel_initialize(void);

amxc_var_t* __wrap_netmodel_getParameters(const char* const interface,
                                          const char* const name,
                                          const char* const flag,
                                          const char* const traverse);
void mock_netmodel_expect_getParameters(const char* const expect_interface,
                                        const char* const expect_name,
                                        const char* const expect_flag,
                                        const char* const expect_traverse,
                                        const char* const return_value);

char* __wrap_netmodel_luckyIntf(const char* const interface,
                                const char* const flag,
                                const char* const traverse);
void mock_netmodel_expect_luckyIntf(const char* const expect_interface,
                                    const char* const expect_flag,
                                    const char* const expect_traverse,
                                    const char* const return_value);

amxc_var_t* __wrap_netmodel_getIntfs(const char* const interface,
                                     const char* const flag,
                                     const char* const traverse);

netmodel_query_t* __wrap_netmodel_openQuery_getIntfs(const char* intf,
                                                     const char* subscriber,
                                                     const char* flag,
                                                     const char* traverse,
                                                     netmodel_callback_t handler,
                                                     void* userdata);
void mock_netmodel_expect_openQuery_getIntfs(const char* expect_intf,
                                             const char* expect_flag,
                                             const char* expect_traverse,
                                             mock_netmodel_query_t* returnvalue);

netmodel_query_t* __wrap_netmodel_openQuery_getAddrs(const char* intf,
                                                     const char* subscriber,
                                                     const char* flag,
                                                     const char* traverse,
                                                     netmodel_callback_t handler,
                                                     void* userdata);
void mock_netmodel_expect_openQuery_getAddrs(const char* expect_intf,
                                             const char* expect_flag,
                                             const char* expect_traverse,
                                             mock_netmodel_query_t* returnvalue);
void mock_netmodel_call_getAddrs_callback(mock_netmodel_query_t* query, const char* data_json_file);

void __wrap_netmodel_closeQuery(netmodel_query_t* query_netmodel);

void mock_netmodel_call_getintf_callback(mock_netmodel_query_t* query, const char* bridge1, const char* bridge2, const char* bridge3);
void mock_netmodel_clear(void);

netmodel_query_t* __wrap_netmodel_openQuery_getParameters(const char* intf,
                                                          const char* subscriber,
                                                          const char* name,
                                                          const char* flag,
                                                          const char* traverse,
                                                          netmodel_callback_t handler,
                                                          void* userdata);
void mock_netmodel_expect_openQuery_getParameters(const char* expect_intf,
                                                  const char* expect_name,
                                                  const char* expect_flag,
                                                  const char* expect_traverse,
                                                  mock_netmodel_query_t* returnvalue);
void mock_netmodel_call_getParameters_callback(mock_netmodel_query_t* query, const char* key, const char* value);

bool __wrap_netmodel_hasFlag(const char* const interface,
                             const char* const flag,
                             const char* const condition,
                             const char* const traverse);

void mock_netmodel_expect_hasFlag(const char* const interface,
                                  const char* const flag,
                                  const char* const condition,
                                  const char* const traverse,
                                  bool returnvalue);

#endif

