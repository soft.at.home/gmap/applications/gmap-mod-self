/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "test-setup.h"
#include "dummy.h"
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxut/amxut_bus.h>
#include "mock_gmap.h"
#include "mock_netmodel.h"

#include <debug/sahtrace.h>

#include <yajl/yajl_gen.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxj/amxj_variant.h>

#include <amxd/amxd_types.h>
#include <amxo/amxo.h>

#include <amxb/amxb.h>
#include <amxc/amxc_macros.h>

#include <gmap/gmap.h>
#include <gmap/gmap_devices_flags.h>

#define DUMMY_TEST_ODL "../common/dummy.odl"

amxo_parser_t* test_setup_parser(void) {
    return amxut_bus_parser();
}

amxd_dm_t* test_setup_dm(void) {
    return amxut_bus_dm();
}

amxb_bus_ctx_t* test_setup_bus_ctx(void) {
    return amxut_bus_ctx();
}

void handle_events(void) {
    amxut_bus_handle_events();
}

/** note: also works for name-based paths, not just index-based */
static amxc_var_t* s_get_param(const char* path, const char* param_name) {
    amxc_var_t params_with_path;
    amxc_var_t* params = NULL;
    amxc_var_t* value = NULL;
    amxc_var_t* value_copy = NULL;
    int rv = 0;
    amxc_var_init(&params_with_path);

    rv = amxb_get(test_setup_bus_ctx(), path, 0, &params_with_path, 1);
    if(rv != 0) {
        fail_msg("Cannot find '%s'", path);
    }
    params = amxc_var_get_first(amxc_var_get_first(&params_with_path));
    value = GET_ARG(params, param_name);
    if(value == NULL) {
        fail_msg("Cannot find parameter '%s' in '%s'", param_name, path);
    }
    amxc_var_new(&value_copy);
    amxc_var_copy(value_copy, value);
    amxc_var_clean(&params_with_path);

    return value_copy;
}

void assert_dm_i32(const char* path, const char* parameter, int32_t expected_value) {
    amxc_var_t* var = s_get_param(path, parameter);
    int32_t actual_value = 0;

    if(AMXC_VAR_ID_INT32 != amxc_var_type_of(var)) {
        fail_msg("Unexpected type for %s.%s : %d", path, parameter, amxc_var_type_of(var));
    }
    actual_value = amxc_var_constcast(int32_t, var);
    assert_int_equal(expected_value, actual_value);
    amxc_var_delete(&var);
}

void assert_dm_ui32(const char* path, const char* parameter, uint32_t expected_value) {
    amxc_var_t* var = s_get_param(path, parameter);
    uint32_t actual_value = 0;
    if(AMXC_VAR_ID_UINT32 != amxc_var_type_of(var)) {
        fail_msg("Unexpected type for %s.%s : %d", path, parameter, amxc_var_type_of(var));
    }

    actual_value = amxc_var_constcast(uint32_t, var);
    assert_int_equal(expected_value, actual_value);
    amxc_var_delete(&var);
}

void assert_dm_str(const char* path, const char* parameter, const char* expected_value) {
    amxc_var_t* var = s_get_param(path, parameter);
    if(AMXC_VAR_ID_CSTRING != amxc_var_type_of(var)) {
        fail_msg("Unexpected type for %s.%s", path, parameter);
    }
    const char* actual_value = amxc_var_constcast(cstring_t, var);
    assert_string_equal(expected_value, actual_value);
    amxc_var_delete(&var);
}

void test_setup_parse_odl(const char* odl_file_name) {
    amxd_object_t* root_obj = amxd_dm_get_root(test_setup_dm());
    if(0 != amxo_parser_parse_file(test_setup_parser(), odl_file_name, root_obj)) {
        fail_msg("PARSER MESSAGE = %s", amxc_string_get(&test_setup_parser()->msg, 0));
    }
}

int test_setup(void** state) {
    amxut_bus_setup(state);

    test_setup_parse_odl(DUMMY_TEST_ODL);

    mock_gmap_clear();

    _dummy_main(0, amxut_bus_dm(), amxut_bus_parser());

    handle_events();

    return 0;
}

int test_teardown(void** state) {
    _dummy_main(1, amxut_bus_dm(), amxut_bus_parser());

    amxut_bus_handle_events();
    amxut_bus_teardown(state);

    mock_gmap_clear();

    return 0;
}

gmap_self_selfdev_t* test_setup_create_selfdev(void) {
    gmap_self_selfdev_t* selfdev = NULL;
    bool ok = false;

    mock_gmap_expect_createDevice("superDuperHGW", "gmap-self", "self protected physical hgw", true, NULL, true);
    mock_gmap_expect_setActive("superDuperHGW", true, true);
    ok = gmap_self_selfdev_new(&selfdev, "superDuperHGW", GMAP_SELF_SELFDEV_DEVTYPE_HGW);
    assert_true(ok);
    assert_non_null(selfdev);
    return selfdev;
}
