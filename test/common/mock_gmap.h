/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef __COMMON_MOCK_GMAP_H__
#define __COMMON_MOCK_GMAP_H__

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxo/amxo.h>
#include <amxb/amxb_register.h>
#include <amxb/amxb_be.h>

void __wrap_gmap_client_init(amxb_bus_ctx_t* bus_ctx);
amxb_bus_ctx_t* __wrap_gmap_get_bus_ctx(void);

bool __wrap_gmap_devices_createDevice(const char* key,
                                      const char* discovery_source,
                                      const char* tags,
                                      bool persistent,
                                      const char* default_name);
void mock_gmap_expect_createDevice(const char* expect_key,
                                   const char* expect_discovery_source,
                                   const char* expect_tags,
                                   bool expect_persistent,
                                   const char* expect_default_name,
                                   bool return_value);

amxc_var_t* __wrap_gmap_device_get(const char* key, uint32_t flags);

bool __wrap_gmap_devices_linkAdd(const char* upper, const char* lower, const char* type, const char* datasource, uint32_t priority);
void mock_gmap_expect_linkAdd(const char* expect_upper, const char* expect_lower,
                              const char* expect_type, const char* expect_datasource, bool return_value);

bool __wrap_gmap_devices_linkReplace(const char* upper,
                                     const char* lower,
                                     const char* type,
                                     const char* datasource,
                                     uint32_t priority);
void mock_gmap_expect_linkReplace(const char* expect_upper, const char* expect_lower, const char* expect_type, const char* datasource, bool return_value);

bool __wrap_gmap_devices_linkRemove(const char* upper, const char* lower, const char* datasource);
void mock_gmap_expect_linkRemove(const char* expect_upper, const char* expect_lower, const char* expect_datasource, bool return_value);

bool __wrap_gmap_device_set(const char* key, amxc_var_t* values);
void mock_gmap_assert_device_param_uint32(const char* device_name, const char* parameter_name, uint32_t expected_value);
void mock_gmap_assert_device_param_string(const char* device_name, const char* parameter_name, const char* expected_value);
void mock_gmap_assert_device_param_bool(const char* device_name, const char* parameter_name, bool expected_value);
void mock_gmap_clear();

bool __wrap_gmap_device_setActive(const char* key, bool value);
void mock_gmap_expect_setActive(const char* expect_key, bool expect_value, bool return_value);

amxd_status_t __wrap_gmap_ip_device_add_address(const char* key, uint32_t family, const char* address,
                                                const char* scope, const char* status_value, const char* address_source, bool reserved);

void mock_gmap_expect_ip_device_add_address(const char* key, uint32_t family, const char* address,
                                            const char* scope, const char* status_value, const char* address_source, bool reserved,
                                            amxd_status_t return_value);
amxd_status_t __wrap_gmap_ip_device_set_address(const char* key, uint32_t family,
                                                const char* address, const char* scope, const char* status_value, const char* address_source,
                                                bool reserved);

void mock_gmap_expect_ip_device_set_address(const char* key, uint32_t family, const char* address,
                                            const char* scope, const char* status_value, const char* address_source, bool reserved,
                                            amxd_status_t return_value);

amxd_status_t __wrap_gmap_ip_device_get_address(const char* key, uint32_t family,
                                                const char* address, amxc_var_t* const ret_object);

void mock_gmap_expect_ip_device_get_address(const char* key, uint32_t family,
                                            const char* address, amxc_var_t* const ret_object);

amxd_status_t __wrap_gmap_ip_device_get_addresses(const char* key, amxc_var_t* target_addresses);

/**
 *
 * @param gmap_addresses_json_file filename containing data in format `{ip1 => { key: value, ...}, ip2 => { ... }, ...}`.
 */
void mock_gmap_ip_device_get_addresses(const char* key, const char* gmap_addresses_json_file);

amxd_status_t __wrap_gmap_ip_device_delete_address(const char* key, uint32_t family, const char* address);

void mock_gmap_ip_device_delete_address(const char* key, uint32_t family, const char* address);

amxc_var_t* __wrap_gmap_config_get(const char* module, const char* option);

#endif

