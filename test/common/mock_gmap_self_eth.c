/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "mock_gmap_self_eth.h"
#include "mock_gmap_self_bridge.h"
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include "test-setup.h"
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxc/amxc_macros.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#define ME "test"

bool __wrap_gmap_self_eth_new(gmap_self_eth_t** eth, gmap_self_bridge_t* bridge, const char* name) {
    mock_gmap_self_eth_t* mock_eth = (mock_gmap_self_eth_t*) mock();

    assert_string_equal(name, mock_eth->eth.name);
    assert_ptr_equal(bridge, mock_eth->eth.bridge);
    assert_non_null(eth);
    if(!mock_eth->fail_on_create) {
        *eth = &mock_eth->eth;
        assert_null(mock_eth->let_valgrind_find_leaks);
        mock_eth->let_valgrind_find_leaks = calloc(1, 1);
        return true;
    } else {
        *eth = NULL;
        return false;
    }
}

void mock_gmap_self_eth_expect_new(mock_gmap_self_eth_t* eth) {
    will_return(__wrap_gmap_self_eth_new, eth);
}

void __wrap_gmap_self_eth_delete(gmap_self_eth_t** self_eth) {
    if((self_eth == NULL) || (*self_eth == NULL)) {
        return;
    }
    mock_gmap_self_eth_t* mock_eth = amxc_container_of(*self_eth, mock_gmap_self_eth_t, eth);
    mock_eth->deletion_count++;

    assert_non_null(mock_eth->let_valgrind_find_leaks);
    free(mock_eth->let_valgrind_find_leaks);
    mock_eth->let_valgrind_find_leaks = NULL;
}

void __wrap_gmap_self_eth_delete_it(const char* name UNUSED, amxc_llist_it_t* it) {
    assert_non_null(it);
    gmap_self_eth_t* eth = amxc_htable_it_get_data(it, gmap_self_eth_t, it);
    assert_non_null(eth);
    // in the "real" `gmap_self_eth_delete_it`, the call to `gmap_self_eth_delete`
    // does not get changed by the linker to `__wrap_gmap_self_eth_delete` (because in
    // the same file, so no linking happening for that call). So do explicit call by ourselves.
    __wrap_gmap_self_eth_delete(&eth);
}

gmap_self_bridge_t* __wrap_gmap_self_eth_bridge(gmap_self_eth_t* eth) {
    assert_non_null(eth);

    mock_gmap_self_eth_t* mock_eth = amxc_container_of(eth, mock_gmap_self_eth_t, eth);
    return (gmap_self_bridge_t*) mock_eth->eth.bridge;
}

void __wrap_gmap_self_eth_set_bridge(gmap_self_eth_t* eth, gmap_self_bridge_t* bridge) {
    check_expected(eth);
    check_expected(bridge);
    eth->bridge = bridge;
}

void mock_gmap_self_eth_expect_set_bridge(mock_gmap_self_eth_t* eth, mock_gmap_self_bridge_t* bridge) {
    expect_value(__wrap_gmap_self_eth_set_bridge, eth, eth);
    expect_value(__wrap_gmap_self_eth_set_bridge, bridge, bridge);
}

const char* __wrap_gmap_self_eth_name(gmap_self_eth_t* eth) {
    assert_non_null(eth);
    return eth->name;
}
