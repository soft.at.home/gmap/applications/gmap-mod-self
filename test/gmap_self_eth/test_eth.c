/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "test_eth.h"
#include "gmap_self_eth.h"
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include "../common/mock_netmodel.h"
#include "../common/mock_gmap.h"
#include "../common/mock_gmap_self_bridge.h"

#include "gmap_self_util.h"
#include "gmap_self_bridge.h"

#include <debug/sahtrace.h>
#include "../common/test-setup.h"
#include <amxd/amxd_object_parameter.h>
#include <amxc/amxc_macros.h>
#include <netmodel/client.h>
#include <amxs/amxs.h>

static void s_create_netmodel_eth_dev() {
    test_setup_parse_odl("../common/data/netmodel-data.odl");
    assert_dm_i32("NetModel.Intf.ethIntf-ETH1.", "CurrentBitRate", 1000);
}

static void s_create_netmodel_vap() {
    test_setup_parse_odl("../common/data/netmodel-data-vap.odl");
}

void test_gmap_self_eth_new__normal_case(UNUSED void** state) {
    // GIVEN an eth in netmodel, a bridge
    bool ok = false;
    gmap_self_eth_t* eth = NULL;
    mock_netmodel_query_t mac_query = {.key = "somedev", .value = "12:34:56:ab:cd:ef" };
    // bridge:
    mock_gmap_self_bridge_t mock_bridge = {.bridge.name = (char*) "bridge-lan_bridge" };
    gmap_self_bridge_t* bridge = (gmap_self_bridge_t*) &mock_bridge.bridge;
    // eth in netmodel:
    mock_netmodel_expect_openQuery_getParameters("ethIntf-ETH1", "MACAddress", NULL, "up", &mac_query);
    s_create_netmodel_eth_dev();

    // EXPECT netmodel will say the interface is not a wifi interface
    mock_netmodel_expect_hasFlag("ethIntf-ETH1", "ssid", NULL, netmodel_traverse_this, false);
    // EXPECT netmodel will say the interface is not a wds interface
    mock_netmodel_expect_hasFlag("ethIntf-ETH1", "wds", NULL, netmodel_traverse_this, false);

    // EXPECT the ethernet port device in gmap to be created, with correct name, flags
    mock_gmap_expect_createDevice("ethIntf-ETH1", "gmap-self", "self lan protected mac eth interface", true, NULL, true);

    // EXPECT "Active" field of device to be set
    mock_gmap_expect_setActive("ethIntf-ETH1", true, true);

    // EXPECT bridge to be parent of ethernet port
    mock_gmap_expect_linkAdd("bridge-lan_bridge", "ethIntf-ETH1", NULL, "mod-self", true);


    // WHEN asking to create the device in gmap
    ok = gmap_self_eth_new(&eth, bridge, "ethIntf-ETH1");
    handle_events();

    // THEN the CurrentBitRate etc fields are synced
    assert_true(ok);
    assert_non_null(eth);
    mock_gmap_assert_device_param_uint32("ethIntf-ETH1", "CurrentBitRate", 1000);
    mock_gmap_assert_device_param_uint32("ethIntf-ETH1", "MaxBitRateSupported", 2500);
    mock_gmap_assert_device_param_string("ethIntf-ETH1", "PhysAddress", "12:34:56:ab:cd:ef");
    mock_gmap_assert_device_param_uint32("ethIntf-ETH1", "NetDevIndex", 11);
    mock_gmap_assert_device_param_string("ethIntf-ETH1", "NetDevName", "eth1");
    mock_gmap_assert_device_param_string("ethIntf-ETH1", "Layer1Interface", "Device.Ethernet.Interface.2.");

    gmap_self_eth_delete(&eth);
}

void test_gmap_self_eth_new__vap(UNUSED void** state) {
    // GIVEN an eth in netmodel, a bridge
    bool ok = false;
    gmap_self_eth_t* eth = NULL;
    mock_netmodel_query_t mac_query = {.key = "somedev", .value = "12:34:56:ab:cd:ef" };
    // bridge:
    mock_gmap_self_bridge_t mock_bridge = {.bridge.name = (char*) "bridge-lan_bridge" };
    gmap_self_bridge_t* bridge = &mock_bridge.bridge;
    // eth in netmodel:
    mock_netmodel_expect_openQuery_getParameters("ssid-vap2g0priv", "MACAddress", NULL, "up", &mac_query);
    s_create_netmodel_vap();

    // EXPECT netmodel will say the interface is a wifi interface
    mock_netmodel_expect_hasFlag("ssid-vap2g0priv", "ssid", NULL, netmodel_traverse_this, true);
    // EXPECT netmodel will say the interface is not a wds interface
    mock_netmodel_expect_hasFlag("ssid-vap2g0priv", "wds", NULL, netmodel_traverse_this, false);

    // EXPECT the vap in gmap to be created, with correct name, flags
    mock_gmap_expect_createDevice("ssid-vap2g0priv", "gmap-self", "self lan protected mac wifi vap interface", true, NULL, true);

    // EXPECT "Active" field of device to be set
    mock_gmap_expect_setActive("ssid-vap2g0priv", true, true);

    // EXPECT bridge to be parent of vap
    mock_gmap_expect_linkAdd("bridge-lan_bridge", "ssid-vap2g0priv", NULL, "mod-self", true);


    // WHEN asking to create the vap in gmap
    ok = gmap_self_eth_new(&eth, bridge, "ssid-vap2g0priv");
    handle_events();

    // THEN the fields are synced
    assert_true(ok);
    assert_non_null(eth);
    mock_gmap_assert_device_param_string("ssid-vap2g0priv", "PhysAddress", "12:34:56:ab:cd:ef");
    mock_gmap_assert_device_param_uint32("ssid-vap2g0priv", "NetDevIndex", 27);
    mock_gmap_assert_device_param_string("ssid-vap2g0priv", "NetDevName", "wlan2");
    mock_gmap_assert_device_param_string("ssid-vap2g0priv", "Layer1Interface", "Device.WiFi.Radio.1");

    gmap_self_eth_delete(&eth);
}

void test_gmap_self_eth_new__create_dev_failed(UNUSED void** state) {
    bool ok = false;
    gmap_self_eth_t* eth = NULL;
    mock_gmap_self_bridge_t mock_bridge = {.bridge.name = (char*) "bridge-lan_bridge" };
    gmap_self_bridge_t* bridge = &mock_bridge.bridge;
    // GIVEN an ethernet port in netmodel
    s_create_netmodel_eth_dev();

    // EXPECT netmodel will say the interface is not a wifi interface
    mock_netmodel_expect_hasFlag("ethIntf-ETH1", "ssid", NULL, netmodel_traverse_this, false);
    // EXPECT netmodel will say the interface is not a wds interface
    mock_netmodel_expect_hasFlag("ethIntf-ETH1", "wds", NULL, netmodel_traverse_this, false);

    // EXPECT the ethernet port gmap device creation to fail
    mock_gmap_expect_createDevice("ethIntf-ETH1", "gmap-self", "self lan protected mac eth interface", true, NULL, false);

    // WHEN asking to create the device in gmap
    ok = gmap_self_eth_new(&eth, bridge, "ethIntf-ETH1");
    assert_null(eth);

    // THEN it failed
    assert_false(ok);

    gmap_self_eth_delete(&eth);
}

void test_gmap_self_eth_set_bridge__replace(UNUSED void** state) {
    bool ok = false;
    gmap_self_eth_t* eth = NULL;
    mock_netmodel_query_t mac_query = {0 };
    mock_gmap_self_bridge_t mock_bridge1 = {.bridge.name = (char*) "bridge1" };
    gmap_self_bridge_t* bridge1 = &mock_bridge1.bridge;
    mock_gmap_self_bridge_t mock_bridge2 = {.bridge.name = (char*) "bridge2" };
    gmap_self_bridge_t* bridge2 = &mock_bridge2.bridge;
    // GIVEN an ethernet port
    s_create_netmodel_eth_dev();
    mock_netmodel_expect_hasFlag("ethIntf-ETH1", "ssid", NULL, netmodel_traverse_this, false);
    // EXPECT netmodel will say the interface is not a wds interface
    mock_netmodel_expect_hasFlag("ethIntf-ETH1", "wds", NULL, netmodel_traverse_this, false);
    mock_gmap_expect_createDevice("ethIntf-ETH1", "gmap-self", "self lan protected mac eth interface", true, NULL, true);
    mock_gmap_expect_linkAdd("bridge1", "ethIntf-ETH1", NULL, "mod-self", true);
    mock_gmap_expect_setActive("ethIntf-ETH1", true, true);
    mock_netmodel_expect_openQuery_getParameters("ethIntf-ETH1", "MACAddress", NULL, "up", &mac_query);
    ok = gmap_self_eth_new(&eth, bridge1, "ethIntf-ETH1");
    assert_true(ok);

    // EXPECT gmap's upper link to change
    mock_gmap_expect_linkRemove("bridge1", "ethIntf-ETH1", "mod-self", true);
    mock_gmap_expect_linkAdd("bridge2", "ethIntf-ETH1", NULL, "mod-self", true);

    // WHEN changing bridge of the eth
    gmap_self_eth_set_bridge(eth, bridge2);

    // THEN the bridge is the new bridge
    assert_ptr_equal(bridge2, gmap_self_eth_bridge(eth));

    gmap_self_eth_delete(&eth);
}

void test_gmap_self_eth_set_bridge__unlink(UNUSED void** state) {
    bool ok = false;
    gmap_self_eth_t* eth = NULL;
    mock_gmap_self_bridge_t mock_bridge1 = {.bridge.name = (char*) "bridge1" };
    mock_netmodel_query_t mac_query = {.key = "somedev", .value = "12:34:56:ab:cd:ef" };
    gmap_self_bridge_t* bridge1 = &mock_bridge1.bridge;
    // GIVEN an ethernet port
    s_create_netmodel_eth_dev();
    mock_netmodel_expect_hasFlag("ethIntf-ETH1", "ssid", NULL, netmodel_traverse_this, false);
    // EXPECT netmodel will say the interface is not a wds interface
    mock_netmodel_expect_hasFlag("ethIntf-ETH1", "wds", NULL, netmodel_traverse_this, false);
    mock_gmap_expect_createDevice("ethIntf-ETH1", "gmap-self", "self lan protected mac eth interface", true, NULL, true);
    mock_gmap_expect_linkAdd("bridge1", "ethIntf-ETH1", NULL, "mod-self", true);
    mock_gmap_expect_setActive("ethIntf-ETH1", true, true);
    mock_netmodel_expect_openQuery_getParameters("ethIntf-ETH1", "MACAddress", NULL, "up", &mac_query);
    ok = gmap_self_eth_new(&eth, bridge1, "ethIntf-ETH1");
    assert_true(ok);

    // EXPECT gmap's upper link to be removed
    mock_gmap_expect_linkRemove("bridge1", "ethIntf-ETH1", "mod-self", true);

    // WHEN changing bridge of the eth to no bridge
    gmap_self_eth_set_bridge(eth, NULL);

    // THEN the eth does not have a bridge anymore
    assert_null(gmap_self_eth_bridge(eth));

    gmap_self_eth_delete(&eth);
}

void test_gmap_self_eth_set_bridge__first_bridge(UNUSED void** state) {
    bool ok = false;
    gmap_self_eth_t* eth = NULL;
    mock_netmodel_query_t mac_query = {0 };
    // GIVEN an ethernet port without bridge, and a bridge
    // bridge:
    mock_gmap_self_bridge_t mock_bridge1 = {.bridge.name = (char*) "bridge1" };
    gmap_self_bridge_t* bridge1 = &mock_bridge1.bridge;
    // eth port without bridge:
    s_create_netmodel_eth_dev();
    mock_netmodel_expect_hasFlag("ethIntf-ETH1", "ssid", NULL, netmodel_traverse_this, false);
    // EXPECT netmodel will say the interface is not a wds interface
    mock_netmodel_expect_hasFlag("ethIntf-ETH1", "wds", NULL, netmodel_traverse_this, false);
    mock_gmap_expect_createDevice("ethIntf-ETH1", "gmap-self", "self lan protected mac eth interface", true, NULL, true);
    mock_gmap_expect_setActive("ethIntf-ETH1", true, true);
    mock_netmodel_expect_openQuery_getParameters("ethIntf-ETH1", "MACAddress", NULL, "up", &mac_query);
    ok = gmap_self_eth_new(&eth, NULL, "ethIntf-ETH1");
    assert_true(ok);
    assert_null(gmap_self_eth_bridge(eth));

    // EXPECT gmap's upper link to be set
    mock_gmap_expect_linkAdd("bridge1", "ethIntf-ETH1", NULL, "mod-self", true);

    // WHEN setting the bridge of the eth
    gmap_self_eth_set_bridge(eth, bridge1);

    // THEN the eth does have this bridge
    assert_ptr_equal(gmap_self_eth_bridge(eth), bridge1);

    gmap_self_eth_delete(&eth);
}

void test_gmap_self_eth_set_bridge__no_op_no_bridge(UNUSED void** state) {
    bool ok = false;
    gmap_self_eth_t* eth = NULL;
    mock_netmodel_query_t mac_query = {.key = "somedev", .value = "12:34:56:ab:cd:ef" };
    // GIVEN an ethernet port without bridge
    // eth port without bridge:
    s_create_netmodel_eth_dev();
    mock_netmodel_expect_hasFlag("ethIntf-ETH1", "ssid", NULL, netmodel_traverse_this, false);
    // EXPECT netmodel will say the interface is not a wds interface
    mock_netmodel_expect_hasFlag("ethIntf-ETH1", "wds", NULL, netmodel_traverse_this, false);
    mock_gmap_expect_createDevice("ethIntf-ETH1", "gmap-self", "self lan protected mac eth interface", true, NULL, true);
    mock_gmap_expect_setActive("ethIntf-ETH1", true, true);
    mock_netmodel_expect_openQuery_getParameters("ethIntf-ETH1", "MACAddress", NULL, "up", &mac_query);
    ok = gmap_self_eth_new(&eth, NULL, "ethIntf-ETH1");
    assert_true(ok);
    assert_null(gmap_self_eth_bridge(eth));

    // WHEN saying again the eth port has no bridge
    gmap_self_eth_set_bridge(eth, NULL);

    // THEN no call to gmap was made and the eth still has no bridge
    assert_null(gmap_self_eth_bridge(eth));

    gmap_self_eth_delete(&eth);
}

void test_gmap_self_eth_set_bridge__no_op_bridge(UNUSED void** state) {
    bool ok = false;
    gmap_self_eth_t* eth = NULL;
    mock_gmap_self_bridge_t mock_bridge1 = {.bridge.name = (char*) "bridge1" };
    mock_netmodel_query_t mac_query = {.key = "somedev", .value = "12:34:56:ab:cd:ef" };
    gmap_self_bridge_t* bridge1 = &mock_bridge1.bridge;
    // GIVEN an ethernet port with a bridge
    s_create_netmodel_eth_dev();
    mock_netmodel_expect_hasFlag("ethIntf-ETH1", "ssid", NULL, netmodel_traverse_this, false);
    // EXPECT netmodel will say the interface is not a wds interface
    mock_netmodel_expect_hasFlag("ethIntf-ETH1", "wds", NULL, netmodel_traverse_this, false);
    mock_gmap_expect_createDevice("ethIntf-ETH1", "gmap-self", "self lan protected mac eth interface", true, NULL, true);
    mock_gmap_expect_linkAdd("bridge1", "ethIntf-ETH1", NULL, "mod-self", true);
    mock_gmap_expect_setActive("ethIntf-ETH1", true, true);
    mock_netmodel_expect_openQuery_getParameters("ethIntf-ETH1", "MACAddress", NULL, "up", &mac_query);
    ok = gmap_self_eth_new(&eth, bridge1, "ethIntf-ETH1");
    assert_true(ok);

    // WHEN telling the eth again it has that bridge
    gmap_self_eth_set_bridge(eth, bridge1);

    // THEN no call to gmap was made and the eth still has that bridge
    assert_ptr_equal(gmap_self_eth_bridge(eth), bridge1);

    gmap_self_eth_delete(&eth);
}

void test_gmap_self_eth_delete_it(UNUSED void** state) {
    // GIVEN a hashtable of eth ports
    test_setup_parse_odl("../common/data/netmodel-data.odl");
    gmap_self_eth_t* eth = NULL;
    mock_netmodel_query_t mac_query = {0 };
    amxc_htable_t htable;
    amxc_htable_init(&htable, 0);
    mock_netmodel_expect_hasFlag("ethIntf-ETH1", "ssid", NULL, netmodel_traverse_this, false);
    // EXPECT netmodel will say the interface is not a wds interface
    mock_netmodel_expect_hasFlag("ethIntf-ETH1", "wds", NULL, netmodel_traverse_this, false);
    mock_gmap_expect_createDevice("ethIntf-ETH1", "gmap-self", "self lan protected mac eth interface", true, NULL, true);
    mock_gmap_expect_setActive("ethIntf-ETH1", true, true);
    mock_netmodel_expect_openQuery_getParameters("ethIntf-ETH1", "MACAddress", NULL, "up", &mac_query);
    gmap_self_eth_new(&eth, NULL, "ethIntf-ETH1");
    assert_non_null(eth);
    amxc_htable_insert(&htable, "eth0", &eth->it);

    // WHEN cleaning up the htable, calling eth iterator cleanup.
    amxc_htable_clean(&htable, gmap_self_eth_delete_it);

    // THEN valgrind finds no leaks
}
