/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include "gmap_self_entrypoint.h"
#include "../common/dummy.h"

#include "../common/mock_netmodel.h"
#include "../common/mock_gmap.h"

#include <netmodel/client.h>

#include "../common/test-setup.h"
#include "test_start_stop.h"
#include <amxc/amxc_macros.h>

void test_can_start_and_stop_plugin(UNUSED void** state) {
    int ret = -1;
    mock_netmodel_query_t netmodel_query = {0};
    amxc_var_add_key(cstring_t, &test_setup_parser()->config, "gmap-mod-self_devtype", "hgw");

    // EXPECT gmap to be initialized
    expect_string(__wrap_gmap_client_init, bus_ctx, test_setup_bus_ctx());

    // EXPECT netmodel to be initialized
    will_return(__wrap_netmodel_initialize, true);

    // EXPECTED selfdev device to be created
    mock_gmap_expect_createDevice("self", "gmap-self", "self protected physical hgw", true, NULL, true);
    mock_gmap_expect_setActive("self", true, true);
    mock_netmodel_expect_openQuery_getIntfs("resolver", "bridge", "all", &netmodel_query);

    // WHEN starting the plugin
    ret = _gmap_self_main(AMXO_START, test_setup_dm(), test_setup_parser());

    // THEN starting was successfully
    assert_int_equal(0, ret);

    // WHEN stopping the plugin
    ret = _gmap_self_main(AMXO_STOP, test_setup_dm(), test_setup_parser());

    // THEN stopping was successfully
    assert_int_equal(0, ret);
}

void test_start_fails_when_create_device_fails(UNUSED void** state) {
    int ret = -1;
    amxc_var_add_key(cstring_t, &test_setup_parser()->config, "gmap-mod-self_devtype", "hgw");
    expect_string(__wrap_gmap_client_init, bus_ctx, test_setup_bus_ctx());
    will_return(__wrap_netmodel_initialize, true);

    // EXPECT device creation to fail
    mock_gmap_expect_createDevice("self", "gmap-self", "self protected physical hgw", true, NULL, false);

    // WHEN starting
    ret = _gmap_self_main(AMXO_START, test_setup_dm(), test_setup_parser());

    // THEN starting failed
    assert_int_not_equal(ret, 0);
}

void test_selfdev_name_is_taken_from_config_option(UNUSED void** state) {
    mock_netmodel_query_t netmodel_query = {0};
    expect_string(__wrap_gmap_client_init, bus_ctx, test_setup_bus_ctx());
    will_return(__wrap_netmodel_initialize, true);

    // GIVEN a configuration where the selfdev name is non-standard
    amxc_var_add_key(cstring_t, &test_setup_parser()->config, "gmap-mod-self_devtype", "hgw");
    amxc_var_add_key(cstring_t, &test_setup_parser()->config, "gmap-mod-self_selfdev-name", "TEST-HGW-NAME");

    // EXPECT the self-device to be created with that non-standard name
    mock_gmap_expect_createDevice("TEST-HGW-NAME", "gmap-self", "self protected physical hgw", true, NULL, true);
    mock_gmap_expect_setActive("TEST-HGW-NAME", true, true);

    mock_netmodel_expect_openQuery_getIntfs("resolver", "bridge", "all", &netmodel_query);

    // WHEN starting
    assert_int_equal(_gmap_self_main(AMXO_START, test_setup_dm(), test_setup_parser()), 0);

    assert_int_equal(_gmap_self_main(AMXO_STOP, test_setup_dm(), test_setup_parser()), 0);
}

void test_selfdev_devtype_is_taken_from_config_option(UNUSED void** state) {
    mock_netmodel_query_t netmodel_query = {0};
    expect_string(__wrap_gmap_client_init, bus_ctx, test_setup_bus_ctx());
    will_return(__wrap_netmodel_initialize, true);

    // GIVEN a configuration where the device type is wifirepeater instead of hgw
    amxc_var_add_key(cstring_t, &test_setup_parser()->config, "gmap-mod-self_devtype", "wifirepeater");

    // EXPECT the self-device to be created without "hgw" tag
    mock_gmap_expect_createDevice("self", "gmap-self", "self protected physical", true, NULL, true);
    mock_gmap_expect_setActive("self", true, true);

    mock_netmodel_expect_openQuery_getIntfs("resolver", "bridge", "all", &netmodel_query);

    // WHEN starting
    assert_int_equal(_gmap_self_main(AMXO_START, test_setup_dm(), test_setup_parser()), 0);

    assert_int_equal(_gmap_self_main(AMXO_STOP, test_setup_dm(), test_setup_parser()), 0);
}

void test_entry_point_ignores_unhandled_reasons(UNUSED void** state) {
    assert_int_equal(_gmap_self_main(99, test_setup_dm(), test_setup_parser()), 0);
}

void test_fails_to_start_when_no_devices_object_found(UNUSED void** state) {
    int ret = -1;

    // GIVEN that there is no "Devices." (the test-setup function did not create that object)

    // WHEN starting
    ret = _gmap_self_main(AMXO_START, test_setup_dm(), test_setup_parser());

    // THEN it failed to start
    assert_int_not_equal(ret, 0);
}
