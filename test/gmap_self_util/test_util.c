/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "test_util.h"
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include "../common/mock_netmodel.h"
#include "../common/mock_gmap.h"

#include "gmap_self_util.h"

#include <debug/sahtrace.h>
#include "../common/test-setup.h"
#include <amxd/amxd_object_parameter.h>
#include <amxc/amxc_macros.h>
#include <netmodel/client.h>

void test_gmap_self_util_get_netmodel_rel_path__normalcase(UNUSED void** state) {
    char* rel_path = NULL;

    // GIVEN a netmodel containing interface "bridge-lan_bridge"
    test_setup_parse_odl("../common/data/netmodel-data.odl");

    // WHEN retrieving the interface's path relative to "NetModel" using index-based paths
    rel_path = gmap_self_util_get_netmodel_rel_path("bridge-lan_bridge");

    // THEN that path is returned
    assert_string_equal("Intf.6.", rel_path);

    free(rel_path);
}

void test_gmap_self_util_get_netmodel_rel_path__invalid_cases(UNUSED void** state) {
    test_setup_parse_odl("../common/data/netmodel-data.odl");

    // No interface exists with name:
    assert_null(gmap_self_util_get_netmodel_rel_path("notfound"));

    // Invalid name:
    assert_null(gmap_self_util_get_netmodel_rel_path(NULL));
    assert_null(gmap_self_util_get_netmodel_rel_path("123 aha aha "));
    assert_null(gmap_self_util_get_netmodel_rel_path("abc.def"));
    assert_null(gmap_self_util_get_netmodel_rel_path(""));
}

void test_gmap_self_util_get_gmap_rel_path(UNUSED void** state) {
    char* rel_path = NULL;

    // normal case:
    rel_path = gmap_self_util_get_gmap_rel_path("my_gmap_name");
    assert_string_equal("Device.my_gmap_name.", rel_path);

    // Invalid argument:
    assert_null(gmap_self_util_get_gmap_rel_path(NULL));
    assert_null(gmap_self_util_get_gmap_rel_path(""));

    free(rel_path);
}

void test_gmap_self_util_netmodel_path_to_intf_name(UNUSED void** state) {
    char* result = NULL;

    result = gmap_self_util_netmodel_path_to_intf_name("NetModel.Intf.eth0");
    assert_string_equal(result, "eth0");
    free(result);

    result = gmap_self_util_netmodel_path_to_intf_name("NetModel.Intf.eth0.");
    assert_string_equal(result, "eth0");
    free(result);

    result = gmap_self_util_netmodel_path_to_intf_name("NetModel.Intf.eth0.Query.123.");
    assert_string_equal(result, "eth0");
    free(result);

    result = gmap_self_util_netmodel_path_to_intf_name("eth0");
    assert_null(result);

    result = gmap_self_util_netmodel_path_to_intf_name("");
    assert_null(result);

    result = gmap_self_util_netmodel_path_to_intf_name(NULL);
    assert_null(result);
}

void test_gmap_self_util_csv_contains_cstring(UNUSED void** state) {
    assert_true(gmap_self_util_csv_contains_cstring("bridge-lan_bridge,bridge-guest_bridge", "bridge-lan_bridge"));
    assert_true(gmap_self_util_csv_contains_cstring("bridge-lan_bridge,bridge-guest_bridge", "bridge-guest_bridge"));
    assert_true(gmap_self_util_csv_contains_cstring("bicycle", "bicycle"));
    assert_true(gmap_self_util_csv_contains_cstring("foo,bar,", ""));
    assert_true(gmap_self_util_csv_contains_cstring(",foo,bar", ""));

    assert_true(gmap_self_util_csv_contains_cstring("foo,,bar", ""));
    assert_false(gmap_self_util_csv_contains_cstring("foo,bar", "foo,"));
    assert_false(gmap_self_util_csv_contains_cstring("foo,bar", "o,b"));
    assert_false(gmap_self_util_csv_contains_cstring("foo,bar", "fo"));
    assert_false(gmap_self_util_csv_contains_cstring("foo,bar", "oo"));
    assert_false(gmap_self_util_csv_contains_cstring("foo,bar", "foo,bar"));
    assert_false(gmap_self_util_csv_contains_cstring("", "foo"));
    assert_false(gmap_self_util_csv_contains_cstring("foo,bar", "foobar"));
    assert_false(gmap_self_util_csv_contains_cstring("foo,bar", ""));
    assert_false(gmap_self_util_csv_contains_cstring("", ""));
    assert_false(gmap_self_util_csv_contains_cstring(NULL, ""));
    assert_false(gmap_self_util_csv_contains_cstring(NULL, "hi"));
    assert_false(gmap_self_util_csv_contains_cstring("foo,bar", NULL));
}