MACHINE = $(shell $(CC) -dumpmachine)

SRCDIR = $(realpath ../../src)
OBJDIR = $(realpath ../../output/$(MACHINE)/coverage)
INCDIR = $(realpath ../../include ../../include_priv)
MOCK_SRCDIR = $(realpath ../common/)

HEADERS = $(wildcard $(INCDIR)/*.h)
SOURCES = $(wildcard $(SRCDIR)/*.c) 
SOURCES += $(wildcard $(MOCK_SRCDIR)/*.c)

CFLAGS += -Werror -Wall -Wextra -Wno-attributes\
          --std=gnu11 -g3 -Wmissing-declarations \
          -Wshadow \
          -Wwrite-strings -Wredundant-decls \
          -Wno-format-nonliteral \
		  $(addprefix -I ,$(INCDIR)) -I$(OBJDIR)/.. \
		  -fkeep-inline-functions -fkeep-static-functions \
		  -Wno-format-nonliteral \
		  $(shell pkg-config --cflags cmocka) -pthread

LDFLAGS += -fkeep-inline-functions -fkeep-static-functions \
		   $(shell pkg-config --libs cmocka) \
		   -lamxc -lamxj -lamxp -lamxd -lamxo -lamxb -lamxs \
		   -lgmap-client -lgmap-ext -lsahtrace -lnetmodel \
		   -ldl -lpthread \
		   -lamxut \

WRAP_FUNC=-Wl,--wrap=

MOCK_WRAP += gmap_devices_linkAdd \
             gmap_devices_linkReplace \
             gmap_devices_linkRemove \
             gmap_devices_createDevice \
             gmap_device_get \
             gmap_device_set \
             gmap_device_setActive \
             gmap_client_init \
             gmap_get_bus_ctx \
             gmap_config_get \
             gmap_ip_device_add_address \
             gmap_ip_device_set_address \
             gmap_ip_device_get_address \
             gmap_ip_device_get_addresses \
             gmap_ip_device_delete_address \
             netmodel_getParameters \
             netmodel_luckyIntf \
             netmodel_getIntfs \
             netmodel_openQuery_getIntfs \
             netmodel_openQuery_getParameters \
             netmodel_openQuery_getAddrs \
             netmodel_closeQuery \
             netmodel_get_amxb_bus \
             netmodel_initialize \
             netmodel_hasFlag \

LDFLAGS += -g $(addprefix $(WRAP_FUNC),$(MOCK_WRAP))