/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#if !defined(__GMAP_SELF_UTIL_H__)
#define __GMAP_SELF_UTIL_H__
#ifdef __cplusplus
extern "C" {
#endif

#include <amxc/amxc.h>
#include <stdbool.h>
#include <amxs/amxs_types.h>
#include <amxc/amxc_macros.h>
#include <netmodel/common_api.h>
#include "gmap_self_selfdev.h"

/**
 * Path relative to "NetModel."/"NeMo." using index-based path.
 *
 * Reason for index-based path (instead of name-based) is because amxs only supports index-based.
 */
char* PRIVATE gmap_self_util_get_netmodel_rel_path(const char* netmodel_name);
char* PRIVATE gmap_self_util_get_gmap_rel_path(const char* name_gmap);
char* PRIVATE gmap_self_util_get_gmap_abs_path(const char* name_gmap);

/**
 * amxs callback that calls gmap's "setActive" instead of writing to "Active".
 */
amxs_status_t PRIVATE gmap_self_util_active_sync_action_cb(const amxs_sync_entry_t* entry,
                                                           amxs_sync_direction_t direction,
                                                           amxc_var_t* data,
                                                           void* priv);

/**
 * amxs callback that calls gmap's "set" instead of writing to fields.
 */
amxs_status_t PRIVATE gmap_self_util_set_action_cb(const amxs_sync_entry_t* entry,
                                                   amxs_sync_direction_t direction,
                                                   amxc_var_t* data,
                                                   void* priv);

/**
 *
 * @param sync_ctx: sync context from "NetModel."/"NeMo." to "Devices."
 *
 * @return The returned sync object must not be freed since it's already added to `sync_ctx`.
 * Returns NULL on error.
 */
amxs_sync_object_t* PRIVATE gmap_self_util_create_sync_obj(amxs_sync_ctx_t* sync_ctx,
                                                           const char* name_netmodel, const char* name_gmap);

/**
 * Searches MAC address in parent interface in netmodel and syncs to gmap.
 *
 * @param intf_name: Name of interface in netmodel whose parent contains mac address, and name
 *   of interface in gmap to which mac address will be synced to.
 *   A pointer to this is kept.
 *   Caller must not free it until returned query is closed.
 * @return query on success, NULL otherwise. Responsibility of closing query is at caller side.
 */
netmodel_query_t* PRIVATE gmap_self_util_start_mac_sync(const char* intf_name);

/**
 * Add a new parameter sync.
 *
 * @param param_to: NULL to use same parameter name as `param_from`.
 */
bool PRIVATE gmap_self_util_sync_ctx_add(amxs_sync_ctx_t* sync_ctx, const char* param_from,
                                         const char* param_to);

bool PRIVATE gmap_self_util_sync_obj_add(amxs_sync_object_t* sync_obj, const char* param_from,
                                         const char* param_to);

char* PRIVATE gmap_self_util_get_netmodel_root_path(void);

bool PRIVATE gmap_self_util_varlist_contains_cstring(const amxc_var_t* list, const char* needle);

bool PRIVATE gmap_self_util_csv_contains_cstring(const char* list_csv, const char* needle);

/** precondition: path is an actual path of an interface in netmodel */
char* PRIVATE gmap_self_util_netmodel_path_to_intf_name(const char* path);

#ifdef __cplusplus
}
#endif
#endif
