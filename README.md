# gMap Mod Self

[[_TOC_]]

## Introduction

The gmap datamodel provides topology information about devices on a network.
The box device itself and its network interfaces are key part of the gmap datamodel. They are managed by the gmap-mod-self module.

gmap-mod-self manages information about:
- the box device itself, i.e. the home gateway itself or the repeater itself. For home gateways this is by default `Devices.Device.self`. This is the root of topology.
- the bridges of the home gateway or repeater, for example `Devices.Device.br-lan` and `Devices.Device.br-guest`.
- the wired interfaces of the home gateway or repeater, i.e. the ethernet ports, for example `Devices.Device.eth0` and `Devices.Device.eth1`.
- the wireless interfaces of the home gateway or repeater

See [./doc/README.md](doc/README.md) for more information and diagrams.

## Building, installing and testing

### Docker container

You could install all tools needed for testing and developing on your local machine, but it is easier to just use a pre-configured environment. Such an environment is already prepared for you as a docker container.

1. Install docker

    Docker must be installed on your system.

    If you have no clue how to do this here are some links that could help you:

    - [Get Docker Engine - Community for Ubuntu](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
    - [Get Docker Engine - Community for Debian](https://docs.docker.com/install/linux/docker-ce/debian/)
    - [Get Docker Engine - Community for Fedora](https://docs.docker.com/install/linux/docker-ce/fedora/)
    - [Get Docker Engine - Community for CentOS](https://docs.docker.com/install/linux/docker-ce/centos/)  <br /><br />
    
    Make sure you user id is added to the docker group:

    ```
    sudo usermod -aG docker $USER
    ```

1. Fetch the container image

    To get access to the pre-configured environment, all you need to do is pull the image and launch a container.

    Pull the image:

    ```bash
    docker pull registry.gitlab.com/soft.at.home/docker/oss-dbg:latest
    ```

    Before launching the container, you should create a directory which will be shared between your local machine and the container.

    ```bash
    mkdir -p ~/amx/gmap
    ```

    Launch the container:

    ```bash
    docker run -ti -d --name oss-dbg --restart always --cap-add=SYS_PTRACE --sysctl net.ipv6.conf.all.disable_ipv6=1 -e "USER=$USER" -e "UID=$(id -u)" -e "GID=$(id -g)" -v ~/amx/:/home/$USER/amx/ registry.gitlab.com/soft.at.home/docker/oss-dbg:latest
    ```

    The `-v` option bind mounts the local directory for the ambiorix project in the container, at the exact same place.
    The `-e` options create environment variables in the container. These variables are used to create a user name with exactly the same user id and group id in the container as on your local host (user mapping).

    You can open as many terminals/consoles as you like:

    ```bash
    docker exec -ti --user $USER oss-dbg /bin/bash
    ```

### Building

#### Prerequisites

- [libamxc](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxc)
- [libamxj](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxj)
- [libamxd](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxd)
- [libamxp](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxp)
- [libamxb](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxb)
- [libamxo](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxo)
- [libgmap-client](https://gitlab.com/prpl-foundation/components/gmap/libraries/libgmap-client)
- [libsahtrace](https://gitlab.com/soft.at.home/logging/libsahtrace)

#### Build gmap-mod-self

1. Clone the git repository

    To be able to build it, you need the source code. So make a directory for the gMap project libraries and clone this library in it.

    ```bash
    mkdir -p ~/workspace/amx/gmap/applications
    cd ~/workspace/amx/gmap/applications
    git clone git@gitlab.com:prpl-foundation/components/gmap/applications/gmap-mod-self.git
    ```

1. Install dependencies

    Although the container will contain all tools needed for building, it does not contain the libraries needed for building `gmap-mod-self`. To be able to build `gmap-mod-self` you need `libamxc`, `libamxj`, `libamxd`, `libamxp`, `libamxb`, `libamxo`, `libamxs`, `libgmap-client`, `libnetmodel` and `libsahtrace`. These libraries can be installed in the container.

    ```bash
    sudo apt update
    sudo apt install libamxj
    sudo apt install libamxb
    sudo apt install libamxo
    sudo apt install libamxs
    sudo apt install libgmap-client
    sudo apt install libnetmodel
    sudo apt install sah-lib-sahtrace-dev
    ```

    Note that you do not need to install all components explicitly. Some components will be installed automatically because the other components depend on them.

1. Build it

   In the container:

    ```bash
    cd ~/workspace/amx/gmap/applications/gmap-mod-self
    make
    ```

### Installing

#### Using make target install

You can install your own compiled version easily in the container by running the install target.

```bash
cd ~/amx/gmap/applications/gmap-mod-self
sudo -E make install
```

#### Using package

From within the container you can create packages.

```bash
cd ~/amx/gmap/applications/gmap-mod-self
make package
```

The packages generated are:

```
~/amx/gmap/applications/gmap-mod-self/gmap-mod-self-<VERSION>.tar.gz
~/amx/gmap/applications/gmap-mod-self/gmap-mod-self-<VERSION>.deb
```

You can copy these packages and extract/install them.

For ubuntu or debian distributions use dpkg:

```bash
sudo dpkg -i ~/amx/gmap/applications/gmap-mod-self/gmap-mod-self-<VERSION>.deb
```

### Testing

#### Prerequisites

Install [libamxut](https://gitlab.com/prpl-foundation/components/ambiorix/tools/libraries/libamxut)
and [amxb_dummy](https://gitlab.com/prpl-foundation/components/ambiorix/modules/amxb_backends/amxb_dummy)

```bash
git clone git@gitlab.com:prpl-foundation/components/ambiorix/tools/libraries/libamxut.git
cd libamxut
make
sudo -E make install
cd ..

git clone git@gitlab.com:prpl-foundation/components/ambiorix/modules/amxb_backends/amxb_dummy.git
cd amxb_dummy
make
sudo -E make install
cd ..
```

#### Run tests

You can run the tests by executing the following command.

```bash
cd ~/amx/gmap/applications/gmap-mod-self/test
make
```

Or this command if you also want the coverage tests to run:

```bash
cd ~/amx/gmap/applications/gmap-mod-self/test
make run && make coverage
```

#### Coverage reports

The coverage target will generate coverage reports using [gcov](https://gcc.gnu.org/onlinedocs/gcc/Gcov.html) and [gcovr](https://gcovr.com/en/stable/guide.html).

A summary for each file (*.c files) is printed in your console after the tests are run.
A HTML version of the coverage reports is also generated. These reports are available in the output directory of the compiler used.
Example: using native gcc
When the output of `gcc -dumpmachine` is `x86_64-linux-gnu`, the HTML coverage reports can be found at `~/amx/gmap/applications/gmap-mod-self/output/x86_64-linux-gnu/coverage/report.`

You can easily access the reports in your browser.
In the container start a python3 http server in background.

```bash
cd ~/amx/
python3 -m http.server 8080 &
```

Use the following url to access the reports `http://<IP ADDRESS OF YOUR CONTAINER>:8080/gmap/applications/gmap-mod-self/output/<MACHINE>/coverage/report`
You can find the ip address of your container by using the `ip` command in the container.

Example:

```bash
USER@<CID>:~/amx/gmap/applications/gmap-mod-self$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
173: eth0@if174: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default 
    link/ether 02:42:ac:11:00:07 brd ff:ff:ff:ff:ff:ff link-netnsid 0
    inet 172.17.0.7/16 scope global eth0
       valid_lft forever preferred_lft forever
    inet6 2001:db8:1::242:ac11:7/64 scope global nodad 
       valid_lft forever preferred_lft forever
    inet6 fe80::42:acff:fe11:7/64 scope link 
       valid_lft forever preferred_lft forever
```

in this case the ip address of the container is `172.17.0.7`.
So the uri you should use is: `http://172.17.0.7:8080/gmap/applications/gmap-mod-self/output/x86_64-linux-gnu/coverage/report/`

## Check

As a sanity check, in the docker container, with `ubus` running, run:

```bash
amxrt -D /etc/amx/gmap-server/gmap-server.odl
```

and

```bash
amxrt -D /etc/amx/gmap-mod-self/gmap-mod-self.odl
```

When you do a `ubus list` you should see the gmap datamodel show up:

```bash
# ubus list
Devices
Devices.Config
Devices.Device
Devices.Query
```

and some additional devices should show up depending on how many interfaces you have on your device:

```bash
Devices.Device.1
Devices.Device.1.Alternative
Devices.Device.1.DeviceTypes
Devices.Device.1.LDevice
Devices.Device.1.Names
Devices.Device.1.UDevice
```

## Startup script
gmap-mod-self will be started, by the startup script of gmap-server
