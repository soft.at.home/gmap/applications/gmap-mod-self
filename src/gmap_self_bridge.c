/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "gmap_self_bridge.h"
#include "gmap_self_util.h"
#include <amxs/amxs.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <gmap/gmap_devices.h>
#include <gmap/extensions/ip/gmap_ext_ipaddress.h>
#include <gmap/extensions/ip/gmap_ext_ip_devices.h>
#include <malloc.h>
#include <string.h>
#include <sys/socket.h>

#define ME "mod-self"
#define LINK_DATASOURCE "mod-self"

/**
 *
 * @param old_gmap_addresses
 *   of the form `{ "192.168.1.1" => { "Family" => "ipv4", ... }, "192.168.123.4" => { ... }, ...}`.
 */
static void s_sync_netmodel_address_to_gmap(const amxc_var_t* old_gmap_addresses, const char* intf_name, const amxc_var_t* netmodel_address) {
    const char* address = GET_CHAR(netmodel_address, "Address");
    const char* family = GET_CHAR(netmodel_address, "Family");
    const char* scope = GET_CHAR(netmodel_address, "Scope");
    amxd_status_t status = amxd_status_unknown_error;
    when_str_empty_trace(address, exit, ERROR, "NULL/Empty");
    when_str_empty_trace(family, exit, ERROR, "NULL/Empty");
    when_str_empty_trace(scope, exit, ERROR, "NULL/Empty");

    // Skip if already exists
    when_not_null(amxc_var_get_key(old_gmap_addresses, address, AMXC_VAR_FLAG_DEFAULT), exit);

    status = gmap_ip_device_add_address(intf_name, gmap_ip_family_string2id(family), address, scope,
                                        GMAP_IP_STATUS_REACHABLE, "self", true);
    when_failed_trace(status, exit, ERROR, "Error adding ip %s to %s", address, intf_name);

exit:
    return;
}

/**
 * @param netmodel_addresses
 *   of the form: `[ { Address = "192.168.1.1", Family = "ipv4", ... }, { Address = "2a02:1802:94:43a0::1", Family = "ipv6", ...}, ...]
 */
static bool s_netmodel_addrlist_has_ip(const amxc_var_t* netmodel_addresses, const char* needle_ip) {
    when_null_trace(netmodel_addresses, error, ERROR, "NULL");
    when_null_trace(needle_ip, error, ERROR, "NULL");

    amxc_var_for_each(haystack_item, netmodel_addresses) {
        const char* haystack_ip = GET_CHAR(haystack_item, "Address");
        when_null_trace(haystack_ip, error, ERROR, "NULL");
        if(0 == strcasecmp(haystack_ip, needle_ip)) {
            return true;
        }
    }
    return false;

error:
    return false;
}

/**
 * Called when netmodel knows and changes the IP address of an interfaces. Sets it in gmap.
 *
 * Implements @ref netmodel_callback_t
 *
 * @param netmodel_addresses
 *   of the form: `[ { Address = "192.168.1.1", Family = "ipv4", ... }, { Address = "2a02:1802:94:43a0::1",  Family = "ipv6", ...}, ...]
 */
static void s_netmodel_ip_cb(const char* sig_name UNUSED, const amxc_var_t* netmodel_addresses, void* priv) {
    const char* intf_name = priv;
    amxd_status_t status = amxd_status_unknown_error;
    amxc_var_t old_gmap_addresses;
    amxc_var_init(&old_gmap_addresses);
    when_null_trace(intf_name, exit, ERROR, "NULL");
    when_null_trace(netmodel_addresses, exit, ERROR, "NULL");

    status = gmap_ip_device_get_addresses(intf_name, &old_gmap_addresses);
    when_failed_trace(status, exit, ERROR, "Error getting current addresses for %s", intf_name);

    // Delete addresses that are not in netmodel anymore
    amxc_var_for_each(old_gmap_address, &old_gmap_addresses) {
        const char* gmap_ip = GET_CHAR(old_gmap_address, "Address");
        if(!s_netmodel_addrlist_has_ip(netmodel_addresses, gmap_ip)) {
            gmap_ip_device_delete_address(intf_name, gmap_ip_family(gmap_ip), gmap_ip);
        }
    }

    // Add addresses that were not in gmap yet.
    amxc_var_for_each(netmodel_address, netmodel_addresses) {
        s_sync_netmodel_address_to_gmap(&old_gmap_addresses, intf_name, netmodel_address);
    }

exit:
    amxc_var_clean(&old_gmap_addresses);
    return;
}

static netmodel_query_t* s_start_ip_sync(const char* intf_name) {
    netmodel_query_t* query = NULL;
    when_str_empty_trace(intf_name, exit, ERROR, "Invalid argument");

    query = netmodel_openQuery_getAddrs(intf_name, "gmap-self", NULL, netmodel_traverse_up, s_netmodel_ip_cb, (void*) intf_name);

    when_null_trace(query, exit, ERROR, "%s: cannot open query", intf_name);

exit:
    return query;
}

/** can only be called once for given bridge */
static bool s_start_sync(gmap_self_bridge_t* bridge) {
    bool retval = false;
    bool ok = false;
    amxs_sync_object_t* sync_obj = NULL;
    amxs_status_t status = amxs_status_unknown_error;
    char* netmodel_path = gmap_self_util_get_netmodel_root_path();
    const char* name = gmap_self_bridge_name(bridge);
    when_null_trace(bridge, exit, ERROR, "NULL");

    status = amxs_sync_ctx_new(&bridge->sync_ctx, netmodel_path, "Devices.", AMXS_SYNC_ONLY_A_TO_B);
    when_failed_trace(status, exit, ERROR, "Error creating sync context");
    sync_obj = gmap_self_util_create_sync_obj(bridge->sync_ctx, name, name);
    when_null_trace(sync_obj, exit, ERROR, "Error creating sync obj");

    status = amxs_sync_object_add_new_param(sync_obj, "Status", "Active", 0, amxs_sync_param_copy_trans_cb, gmap_self_util_active_sync_action_cb, NULL);
    when_failed_trace(status, exit, ERROR, "Failed add sync param Status->Active for '%s'", name);
    ok = gmap_self_util_sync_obj_add(sync_obj, "NetDevIndex", NULL);
    ok &= gmap_self_util_sync_obj_add(sync_obj, "NetDevName", NULL);
    when_false_trace(ok, exit, ERROR, "Failed add sync param(s) for '%s'", name);

    status = amxs_sync_ctx_start_sync(bridge->sync_ctx);
    when_failed_trace(status, exit, ERROR, "Failed start sync '%s'", name);

    // Sync MAC:
    bridge->query_mac_sync = gmap_self_util_start_mac_sync(name);
    when_null_trace(bridge->query_mac_sync, exit, ERROR, "%s: Error starting mac sync", name);

    // Sync IP:
    bridge->query_ip_sync = s_start_ip_sync(name);
    when_null_trace(bridge->query_ip_sync, exit, ERROR, "%s: Error starting mac sync", name);

    retval = true;
exit:
    free(netmodel_path);
    return retval;
}

/** can only be called once for a given bridge */
static bool s_start(gmap_self_bridge_t* bridge, gmap_self_selfdev_t* selfdev) {
    bool ok = false;
    const char* name = gmap_self_bridge_name(bridge);

    // Create gmap dev:
    ok = gmap_devices_createDevice(name, "gmap-self", "self lan protected mac interface bridge ipv4 ipv6", true, NULL);
    when_false_trace(ok, error, ERROR, "Failed to create bridge '%s'", name);
    SAH_TRACEZ_INFO(ME, "Created bridge '%s'", name);

    // Link to parent/upper:
    const char* selfdev_name = gmap_self_selfdev_name(selfdev);
    ok = gmap_devices_linkAdd(selfdev_name, name, NULL, LINK_DATASOURCE, 0);
    when_false_trace(ok, error, ERROR, "Error linking '%s' to '%s'", selfdev_name, name);

    // Sync params:
    ok = s_start_sync(bridge);
    when_false_trace(ok, error, ERROR, "Error syncing params for '%s'", name);

    return true;
error:
    return false;
}

bool gmap_self_bridge_new(gmap_self_bridge_t** bridge, gmap_self_selfdev_t* selfdev, const char* name) {
    bool ok = false;
    when_null_trace(bridge, error, ERROR, "NULL");
    when_null_trace(selfdev, error, ERROR, "NULL");
    when_str_empty_trace(name, error, ERROR, "Invalid argument");

    *bridge = calloc(1, sizeof(gmap_self_bridge_t));
    when_null_trace(*bridge, error, ERROR, "Out of mem");

    (*bridge)->name = strdup(name);
    when_null_trace((*bridge)->name, error, ERROR, "Out of mem");

    ok = s_start(*bridge, selfdev);
    when_false_trace(ok, error, ERROR, "Error start device creation or syncing");

    return true;
error:
    gmap_self_bridge_delete(bridge, false);
    return false;
}

void gmap_self_bridge_delete(gmap_self_bridge_t** bridge, bool also_destroy_in_gmap) {
    if((bridge == NULL) || (*bridge == NULL)) {
        return;
    }

    netmodel_closeQuery((*bridge)->query_mac_sync);
    (*bridge)->query_mac_sync = NULL;

    netmodel_closeQuery((*bridge)->query_ip_sync);
    (*bridge)->query_ip_sync = NULL;

    amxs_sync_ctx_delete(&(*bridge)->sync_ctx);

    if(((*bridge)->name != NULL) && also_destroy_in_gmap) {
        gmap_devices_destroyDevice((*bridge)->name);
    }

    free((*bridge)->name);

    free(*bridge);
    *bridge = NULL;
}

const char* gmap_self_bridge_name(gmap_self_bridge_t* bridge) {
    when_null_trace(bridge, error, ERROR, "NULL");
    return bridge->name;
error:
    return NULL;
}
