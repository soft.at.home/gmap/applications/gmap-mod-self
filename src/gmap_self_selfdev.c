/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "gmap_self_selfdev.h"
#include "gmap_self_util.h"
#include <amxs/amxs.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <malloc.h>
#include <string.h>

#define ME "mod-self"
#define ACTIVE_SOURCE "gmap-mod-self"
#define ACTIVE_PRIORITY 100

struct gmap_self_selfdev {
    /** never null/empty */
    char* name;
    /** from "DeviceInfo." to "Devices.Device.<name>." */
    amxs_sync_ctx_t* sync_ctx;
};

static bool s_init_sync_ctx(gmap_self_selfdev_t* selfdev) {
    amxc_string_t path;
    amxs_status_t status = amxs_status_unknown_error;
    bool retval = false;
    amxc_string_init(&path, 20);
    when_null_trace(selfdev, exit, ERROR, "NULL");
    when_str_empty_trace(gmap_self_selfdev_name(selfdev), exit, ERROR, "No name");
    when_false_trace(selfdev->sync_ctx == NULL, exit, ERROR, "Sync ctx already created");

    amxc_string_setf(&path, "Devices.Device.%s.", gmap_self_selfdev_name(selfdev));

    status = amxs_sync_ctx_new(&selfdev->sync_ctx, "DeviceInfo.", amxc_string_get(&path, 0), AMXS_SYNC_ONLY_A_TO_B);
    when_failed_trace(status, exit, ERROR, "Failed to create sync ctx for selfdev '%s': %d", gmap_self_selfdev_name(selfdev), status);

    retval = true;
exit:
    amxc_string_clean(&path);
    return retval;
}

static bool s_start_sync_params(gmap_self_selfdev_t* selfdev) {
    bool ok = false;
    bool retval = false;
    amxs_status_t status = amxs_status_unknown_error;

    ok = s_init_sync_ctx(selfdev);
    when_false_trace(ok, exit, ERROR, "Error creating sync ctx");

    ok = gmap_self_util_sync_ctx_add(selfdev->sync_ctx, "Manufacturer", NULL);
    ok &= gmap_self_util_sync_ctx_add(selfdev->sync_ctx, "ModelName", NULL);
    ok &= gmap_self_util_sync_ctx_add(selfdev->sync_ctx, "Description", NULL);
    ok &= gmap_self_util_sync_ctx_add(selfdev->sync_ctx, "SerialNumber", NULL);
    ok &= gmap_self_util_sync_ctx_add(selfdev->sync_ctx, "HardwareVersion", NULL);
    ok &= gmap_self_util_sync_ctx_add(selfdev->sync_ctx, "SoftwareVersion", NULL);
    ok &= gmap_self_util_sync_ctx_add(selfdev->sync_ctx, "ProductClass", NULL);
    when_false_trace(ok, exit, ERROR, "Failed to add one or more sync params");

    status = amxs_sync_ctx_start_sync(selfdev->sync_ctx);
    when_failed_trace(status, exit, ERROR, "Failed to start param sync for selfdev %s: %d", gmap_self_selfdev_name(selfdev), status);

    retval = true;
exit:
    return retval;
}

static const char* s_get_flags(gmap_self_selfdev_devtype_e devtype) {
    switch(devtype) {
    case GMAP_SELF_SELFDEV_DEVTYPE_HGW:
        return "self protected physical hgw";
    case GMAP_SELF_SELFDEV_DEVTYPE_WIFIREPEATER:
        return "self protected physical";
    case GMAP_SELF_SELFDEV_DEVTYPE_INVALID:
    case GMAP_SELF_SELFDEV_DEVTYPE_MAX:
        SAH_TRACEZ_ERROR(ME, "Invalid devtype");
        return NULL;
    }

    _Static_assert(GMAP_SELF_SELFDEV_DEVTYPE_MAX == 3, "Update case analysis in code above when adding device types");
    return NULL;
}

static bool s_start(gmap_self_selfdev_t* selfdev, gmap_self_selfdev_devtype_e devtype) {
    bool ok = false;
    bool retval = false;
    const char* flags = s_get_flags(devtype);
    when_null_trace(flags, exit, ERROR, "Cannot determine flags");

    // Create the selfdev device in GMap Server
    ok = gmap_devices_createDevice(selfdev->name, "gmap-self", flags, true, NULL);
    when_false_trace(ok, exit, ERROR, "Failed to create selfdev '%s'", selfdev->name);
    SAH_TRACEZ_INFO(ME, "Created selfdev '%s'", selfdev->name);

    ok = gmap_device_setActive(selfdev->name, true, ACTIVE_SOURCE, ACTIVE_PRIORITY);
    if(!ok) {
        SAH_TRACEZ_ERROR(ME, "Error setting Active of '%s'", selfdev->name);
    }

    // start data sync
    ok = s_start_sync_params(selfdev);
    when_false_trace(ok, exit, ERROR, "Error starting selfdev param sync");

    retval = true;
exit:
    return retval;
}

bool gmap_self_selfdev_new(gmap_self_selfdev_t** selfdev, const char* name, gmap_self_selfdev_devtype_e devtype) {
    bool retval = false;
    when_null_trace(selfdev, error, ERROR, "NULL");
    when_str_empty_trace(name, error, ERROR, "EMPTY");
    when_false_trace(devtype != GMAP_SELF_SELFDEV_DEVTYPE_INVALID, error, ERROR, "Invalid devtype");

    *selfdev = calloc(1, sizeof(gmap_self_selfdev_t));
    when_null_trace(*selfdev, error, ERROR, "Out of mem");

    (*selfdev)->name = strdup(name);
    when_null_trace((*selfdev)->name, error, ERROR, "Out of mem");

    bool ok = s_start(*selfdev, devtype);
    when_false_trace(ok, error, ERROR, "Cannot start syncing/creating");

    retval = true;
    goto cleanup;
error:
    gmap_self_selfdev_delete(selfdev);
cleanup:
    return retval;
}

void gmap_self_selfdev_delete(gmap_self_selfdev_t** selfdev) {
    if((selfdev == NULL) || (*selfdev == NULL)) {
        return;
    }

    amxs_sync_ctx_delete(&(*selfdev)->sync_ctx);

    free((*selfdev)->name);
    free(*selfdev);
    *selfdev = NULL;
}

const char* gmap_self_selfdev_name(gmap_self_selfdev_t* selfdev) {
    when_null_trace(selfdev, error, ERROR, "NULL");
    return selfdev->name;

error:
    return NULL;
}

gmap_self_selfdev_devtype_e gmap_self_selfdev_devtype_of_string(const char* string) {
    when_null_trace(string, error, ERROR, "No devtype string given");
    if(0 == strcmp("hgw", string)) {
        return GMAP_SELF_SELFDEV_DEVTYPE_HGW;
    } else if(0 == strcmp("wifirepeater", string)) {
        return GMAP_SELF_SELFDEV_DEVTYPE_WIFIREPEATER;
    }

    SAH_TRACEZ_ERROR(ME, "Unknown device type '%s'", string);
error:
    return GMAP_SELF_SELFDEV_DEVTYPE_INVALID;
}
