/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/


#include "gmap_self_entrypoint.h"
#include "gmap_self_selfdev.h"
#include "gmap_self.h"
#include <amxb/amxb.h>
#include <gmap/gmap.h>
#include <debug/sahtrace.h>
#include <netmodel/client.h>
#include <debug/sahtrace_macros.h>

#define ME "mod-self"

static gmap_self_t* s_gmap_self = NULL;

/** Return "gmap-mod-self_selfdev-name" configuration field contents, or "self" if missing. */
static const char* s_selfdev_name(amxo_parser_t* parser) {
    const char* selfdev_name = amxc_var_constcast(cstring_t, amxo_parser_get_config(parser, "gmap-mod-self_selfdev-name"));
    if((selfdev_name == NULL) || (selfdev_name[0] == '\0')) {
        selfdev_name = "self";
    }
    return selfdev_name;
}

static int s_cleanup(void) {
    gmap_self_delete(&s_gmap_self);
    netmodel_cleanup();
    return 0;
}

static int s_init(UNUSED amxd_dm_t* dm,
                  amxo_parser_t* parser) {
    bool success = false;
    int retval = -1;
    amxb_bus_ctx_t* ctx = NULL;
    gmap_self_selfdev_devtype_e devtype = GMAP_SELF_SELFDEV_DEVTYPE_INVALID;
    if(s_gmap_self != NULL) {
        SAH_TRACEZ_ERROR(ME, "Double init");
        return -1; // don't do any cleanup on double init
    }

    // Init gmap:
    ctx = amxb_be_who_has("Devices.Device");
    when_null_trace(ctx, error, ERROR, "gMap data model not found 'Devices.Device', is gmap-server running?");
    gmap_client_init(ctx);

    // Init netmodel:
    success = netmodel_initialize();
    when_false_trace(success, error, ERROR, "Could not initialize lib_netmodel");

    // Init and start gmap_self:
    devtype = gmap_self_selfdev_devtype_of_string(amxc_var_constcast(cstring_t, amxo_parser_get_config(parser, "gmap-mod-self_devtype")));
    when_false_trace(devtype != GMAP_SELF_SELFDEV_DEVTYPE_INVALID, error, ERROR, "Cannot get devtype");
    success = gmap_self_new(&s_gmap_self, s_selfdev_name(parser), devtype);
    when_false_trace(success, error, ERROR, "Error creating/starting gmap_self");

    retval = 0;
    goto leave;
error:
    s_cleanup();
leave:
    return retval;
}


int _gmap_self_main(int reason,
                    amxd_dm_t* dm,
                    amxo_parser_t* parser) {

    int retval = 0;
    switch(reason) {
    case AMXO_START:
        retval = s_init(dm, parser);
        break;
    case AMXO_STOP:
        retval = s_cleanup();
        break;
    default:
        // not handled reason - should not fail
        break;
    }

    return retval;
}

