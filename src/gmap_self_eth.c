/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "gmap_self_eth.h"
#include "gmap_self_util.h"
#include "gmap_self_bridge.h"
#include <string.h>
#include <amxc/amxc.h>
#include <amxs/amxs.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <netmodel/common_api.h>
#include <malloc.h>
#include <amxc/amxc_macros.h>
#include <gmap/gmap_devices.h>

#define ME "mod-self"
#define LINK_DATASOURCE "mod-self"


static amxs_status_t interfacepath_param_cb(UNUSED const amxs_sync_entry_t* entry,
                                            UNUSED amxs_sync_direction_t direction,
                                            amxc_var_t* data,
                                            void* priv) {
    amxc_string_t wifi_path;
    amxs_status_t ret = amxs_status_ok;
    gmap_self_eth_t* eth = (gmap_self_eth_t*) priv;
    char* gmap_path = gmap_self_util_get_gmap_abs_path(eth->name);

    amxc_string_init(&wifi_path, 0);
    amxc_string_set(&wifi_path, GETP_CHAR(data, "parameters.Layer1Interface"));
    amxc_string_replace(&wifi_path, "Device.", "", 1);

    if(eth->sync_ctx_wifi) {
        amxs_sync_ctx_delete(&(eth->sync_ctx_wifi));
        eth->sync_ctx_wifi = NULL;
    }

    ret = amxs_sync_ctx_new(&(eth->sync_ctx_wifi), amxc_string_get(&wifi_path, 0), gmap_path, AMXS_SYNC_ONLY_A_TO_B);
    ret |= amxs_sync_ctx_add_new_param(eth->sync_ctx_wifi, "LowerLayers", "Layer1Interface", 0, amxs_sync_param_copy_trans_cb, gmap_self_util_set_action_cb, NULL);
    when_failed_trace(ret, exit, ERROR, "amxs_sync failed for '%s%s' to '%s%s'", amxc_string_get(&wifi_path, 0), "LowerLayers", gmap_path, "Layer1Interface");

    ret |= amxs_sync_ctx_start_sync(eth->sync_ctx_wifi);
    when_failed_trace(ret, exit, ERROR, "Failed starting sync");

exit:
    if((ret != amxs_status_ok) && eth->sync_ctx_wifi) {
        amxs_sync_ctx_delete(&(eth->sync_ctx_wifi));
        eth->sync_ctx_wifi = NULL;
    }
    amxc_string_clean(&wifi_path);
    free(gmap_path);

    return ret;
}

static bool s_start_sync_params(gmap_self_eth_t* eth, bool is_wifi) {
    bool retval = false;
    bool ok = true;
    amxs_status_t status = amxs_status_unknown_error;
    amxs_sync_object_t* sync_obj = NULL;
    char* netmodel_path = gmap_self_util_get_netmodel_root_path();
    const char* eth_name = gmap_self_eth_name(eth);

    status = amxs_sync_ctx_new(&eth->sync_ctx, netmodel_path, "Devices.", AMXS_SYNC_ONLY_A_TO_B);
    when_failed_trace(status, exit, ERROR, "Error creating sync context");
    sync_obj = gmap_self_util_create_sync_obj(eth->sync_ctx, eth_name, eth_name);
    when_null_trace(sync_obj, exit, ERROR, "Error creating sync obj");

    status = amxs_sync_object_add_new_param(sync_obj, "Status", "Active", 0, amxs_sync_param_copy_trans_cb, gmap_self_util_active_sync_action_cb, NULL);
    when_failed_trace(status, exit, ERROR, "Failed add param sync Status->Active on '%s'", eth_name);

    if(!is_wifi) {
        ok = ok && gmap_self_util_sync_obj_add(sync_obj, "CurrentBitRate", NULL);
        ok = ok && gmap_self_util_sync_obj_add(sync_obj, "MaxBitRateEnabled", "MaxBitRateSupported");
    }
    ok = ok && gmap_self_util_sync_obj_add(sync_obj, "NetDevIndex", NULL);
    ok = ok && gmap_self_util_sync_obj_add(sync_obj, "NetDevName", NULL);
    when_false_trace(ok, exit, ERROR, "Failed add sync param(s) on '%s'", eth_name);

    if(is_wifi) {
        ok = ok && (amxs_status_ok == amxs_sync_object_add_new_param(sync_obj, "InterfacePath", "Layer1Interface", 0, amxs_sync_param_copy_trans_cb, interfacepath_param_cb, eth));
    } else {
        ok = ok && gmap_self_util_sync_obj_add(sync_obj, "InterfacePath", "Layer1Interface");
    }
    when_false_trace(ok, exit, ERROR, "Failed add sync Layer1Interface on '%s'", eth_name);

    status = amxs_sync_ctx_start_sync(eth->sync_ctx);
    when_failed_trace(status, exit, ERROR, "Failed starting sync");

    // Sync MAC:
    eth->query_mac_sync = gmap_self_util_start_mac_sync(eth_name);
    when_null_trace(eth->query_mac_sync, exit, ERROR, "%s: Error starting mac sync", eth_name);

    retval = true;
exit:
    if((!ok || (status != amxs_status_ok)) && eth->sync_ctx) {
        amxs_sync_ctx_delete(&(eth->sync_ctx));
        eth->sync_ctx = NULL;
    }
    free(netmodel_path);

    return retval;
}

bool gmap_self_eth_new(gmap_self_eth_t** eth, gmap_self_bridge_t* bridge, const char* name) {
    amxc_string_t tags;
    bool ok = false;
    int status = -1;
    bool is_wifi = false;
    amxc_string_init(&tags, 32);
    when_null_trace(eth, error, ERROR, "NULL");
    when_str_empty_trace(name, error, ERROR, "Invalid argument");

    *eth = calloc(1, sizeof(gmap_self_eth_t));
    when_null_trace(*eth, error, ERROR, "Out of mem");
    (*eth)->name = strdup(name);
    when_null_trace((*eth)->name, error, ERROR, "Out of mem");

    is_wifi = netmodel_hasFlag(name, "ssid", NULL, netmodel_traverse_this);
    is_wifi |= netmodel_hasFlag(name, "wds", NULL, netmodel_traverse_this);

    status = amxc_string_setf(&tags, "self lan protected mac %s interface", is_wifi ? "wifi vap" : "eth");
    when_failed_trace(status, error, ERROR, "Out of mem");

    // Create:
    ok = gmap_devices_createDevice(name, "gmap-self", amxc_string_get(&tags, 0), true, NULL);
    when_false_trace(ok, error, ERROR, "Failed to create eth '%s'", name);
    SAH_TRACEZ_INFO(ME, "Created eth '%s'", name);

    // Link to parent/upper:
    if(bridge != NULL) {
        gmap_self_eth_set_bridge(*eth, bridge);
    }

    // Start sync:
    ok = s_start_sync_params(*eth, is_wifi);
    when_false_trace(ok, error, ERROR, "Error setting up param sync for %s", name);

    amxc_string_clean(&tags);
    return true;
error:
    amxc_string_clean(&tags);
    gmap_self_eth_delete(eth);
    return false;
}

void gmap_self_eth_delete(gmap_self_eth_t** eth) {
    if((eth == NULL) || (*eth == NULL)) {
        return;
    }

    netmodel_closeQuery((*eth)->query_mac_sync);
    (*eth)->query_mac_sync = NULL;

    if((*eth)->sync_ctx_wifi) {
        amxs_sync_ctx_delete(&(*eth)->sync_ctx_wifi);
    }
    amxs_sync_ctx_delete(&(*eth)->sync_ctx);

    free((*eth)->name);

    free(*eth);
    *eth = NULL;
}

void gmap_self_eth_delete_it(const char* key UNUSED, amxc_htable_it_t* it) {
    when_null_trace(it, exit, ERROR, "NULL");
    gmap_self_eth_t* eth = amxc_htable_it_get_data(it, gmap_self_eth_t, it);
    when_null_trace(eth, exit, ERROR, "NULL");
    gmap_self_eth_delete(&eth);
exit:
    return;
}

gmap_self_bridge_t* gmap_self_eth_bridge(gmap_self_eth_t* eth) {
    when_null_trace(eth, error, ERROR, "NULL");
    return eth->bridge;
error:
    return NULL;
}

void gmap_self_eth_set_bridge(gmap_self_eth_t* eth, gmap_self_bridge_t* new_bridge) {
    when_null_trace(eth, exit, ERROR, "NULL");
    gmap_self_bridge_t* old_bridge = gmap_self_eth_bridge(eth);

    when_true(old_bridge == new_bridge, exit); // no-op.

    if(old_bridge != NULL) {
        gmap_devices_linkRemove(gmap_self_bridge_name(old_bridge), gmap_self_eth_name(eth), LINK_DATASOURCE);
    }
    if(new_bridge != NULL) {
        gmap_devices_linkAdd(gmap_self_bridge_name(new_bridge), gmap_self_eth_name(eth), NULL, LINK_DATASOURCE, 0);
    }
    eth->bridge = new_bridge;

exit:
    return;
}

const char* gmap_self_eth_name(gmap_self_eth_t* eth) {
    when_null_trace(eth, error, ERROR, "NULL");
    return eth->name;
error:
    return NULL;
}
