# gmap-mod-self
## Responsibility

gmap-mod-self is a component that is part of the collection of components that we call gmap.

gmap is responsible for modeling the network topology and updating this model. gmap-mod-self takes care of a part of this model of the topology.

An example of that part is shown in the diagram below.

![example topology](./diagrams/topology.drawio.svg)

The part of the topology that gmap-mod-self is responsible for is:
- the self device (the device on which gmap is running, typically a homegateway or a wifi repeater). This is the root of the topology.
- the bridges on the self device
- the ports that are part of the bridges (ethernet ports and wifi accesspoints)
- data about the self-device, the bridges, and the ports (MAC address, etc)
- the relationships between those

For all of these, gmap-mod-self supports when they change. For example
- when a port is added
- when a port moves to another bridge
- when a port is deleted
- when a bridge is added
- when a bridge is deleted
- when a bridge changes MAC address
- when data about the self device (e.g. manufacturer) is only filled in a while after boot
- etc.

The topology as discovered by gmap-mod-self can be queried as follows (for brevity only selected lines are shown):
```
root@prplOS:/# ba-cli
[...]
 - * - [bus-cli] (0)
 > Devices.Device.[DiscoverySource=="gmap-self"].?
Devices.Device.1.
Devices.Device.1.Alias="self"
Devices.Device.1.Name="HGW"
Devices.Device.1.PhysAddress="E6:11:59:4A:19:F1"
Devices.Device.1.LDevice.1.Alias="bridge-lan_bridge"
Devices.Device.1.LDevice.2.Alias="bridge-guest_bridge"
[...]
Devices.Device.2.
Devices.Device.2.Alias="bridge-lan_bridge"
Devices.Device.2.IPAddress="192.168.1.1"
Devices.Device.2.LDevice.1.Alias="ethIntf-ETH1"
Devices.Device.2.UDevice.1.Alias="self"
[...]
Devices.Device.3.
Devices.Device.3.Active=1
Devices.Device.3.Alias="ethIntf-ETH1"
Devices.Device.3.UDevice.1.Alias="bridge-lan_bridge"
[...]
```

Or, alternatively (also includes topology information gathered gmap modules different from gmap-mod-self):
```
 - * - [bus-cli] (0)
 > Devices.Device.self.topology()
Devices.Device.self.topology() returned
[
    {
        Alias = "self",
        // ...
        Children = [
            {
                Alias = "bridge-lan_bridge",
                IPAddress = "192.168.1.1",
                Children = [
                    {
                        Alias = "ethIntf-ETH1",
                        // ...
        // ...
```
 
## Relation to other components

![relation to other components](./diagrams/gmap-mod-self-highlevel.drawio.svg)

[gmap-client](https://gitlab.com/prpl-foundation/components/gmap/applications/gmap-client) starts the gmap modules such as gmap-mod-self, [gmap-mod-ethernet-dev](https://gitlab.com/prpl-foundation/components/gmap/applications/gmap-mod-ethernet-dev/), etc.

gmap-mod-self retrieves all its data (indirectly) from the datamodel of other components:
- [tr181-deviceinfo](https://gitlab.com/prpl-foundation/components/core/plugins/tr181-deviceinfo/)
- [NetModel](https://gitlab.com/prpl-foundation/components/core/plugins/netmodel/)

gmap-mod-self does not have a datamodel of its own. Instead, it feeds the datamodel of [gmap-server](https://gitlab.com/prpl-foundation/components/gmap/applications/gmap-server), who publishes its datamodel under the path `Devices.`. gmap-mod-self does not write to gmap-server's datamodel directly. Instead, it either syncs to it via the synchronization library [libamxs](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxs), or uses [libgmap-client](https://gitlab.com/soft.at.home/ambx/gmap/libraries/libgmap-client) which provides a C API to update gmap-server's datamodel.

While all of the data gathered by gmap-mod-self comes from other components, it does not read from the datamodel of those components directly. Instead, it either syncs from it via the syncronisation library [libamxs](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxs), or uses [libnetmodel](https://gitlab.com/prpl-foundation/components/core/libraries/lib_netmodel/) which provides a C API to read and query NetModel's datamodel.

## The self device

![The self device](./diagrams/dataflow-gmap-mod-self-selfdev.drawio.svg)

The homegateway, repeater, or other physical device on which gmap is running, is in gmap's topology represented by a gmap device (i.e. a node in gmap's model of the topology) that we call "self", "the self device" or "the self gmap-device".

gmap-mod-self creates this gmap device.

When gmap-mod-self creates this self device, this new gmap device has the tags `self protected physical` and usually also `hgw`. In other words: `Devices.Devices.self.Tags` = `self protected physical hgw`. The `hgw` tag is included except if the config option `CONFIG_SAH_SERVICES_GMAP_SELF_DEVTYPE_WIFIREPEATER` is set. This config option indicates the device on which gmap is running is a wifi repeater and not a homegateway.

gmap-mod-self also syncs properties about the self device from DeviceInfo to gmap-server: see the diagram above.


## Bridges

![Bridges](./diagrams/dataflow-gmap-mod-self-bridges.drawio.svg)

Bridges can be created and deleted while gmap is running. gmap-mod-self opens a query to Netmodel to be informed when that happens.

Since both NetModel and gmap model bridges, we'll call bridges modeled in netmodel a "netmodel bridge" and a bridge modeled in gmap a "gmap bridge".

When NetModel informs gmap-mod-self of the existance of an already-existing netmodel bridge (at startup of gmap-mod-self) or of a new netmodel bridge (after startup), gmap-mod-self creates a gmap bridge in gmap-server (i.e. it creates `Devices.Device.<bridge>.`), and lets libamxs sync parameters from netmodel to gmap-server, as shown in the diagram above.

Furthermore, the gmap bridges are linked to the self-device (i.e. gmap-mod-self creates `Devices.Device.self.LDevice.<bridge>` and `Devices.Device.<bridge>.UDevice.self`), so that the gmap bridges appear at the correct place in the [topology](#responsibility).

To avoid to model bridges in gmap that only exist for technical reasons but are not considered part of the topology (such as an LCM bridge), there is a whitelist configured of bridges that gmap-mod-self is allowed to create in gmap. This configuration can be queried as follows:
```
root@prplOS:/# ba-cli
[...]
 - * - [bus-cli] (0)
 > Devices.Config.get(module = "mod-self", option="Bridges")
Devices.Config.get() returned
[
    "bridge-lan_bridge,bridge-guest_bridge"
]
```

When gmap-mod-self creates a gmap bridge, this new gmap device has the the tags `self lan protected mac interface bridge ipv4 ipv6`.

## Ports (eth and wifi)

![Bridges](./diagrams/dataflow-gmap-mod-self-ports.drawio.svg)

When gmap-mod-self adds a gmap bridge, it opens a query on NetModel. This query informs gmap-mod-self of
- the already-existing netmodel ports attached to the netmodel bridge (i.e. netmodel ports that already exist from before the query is opened),
- netmodel ports that come into existance at a later point in time
- whenever a netmodel port is removed from the bridge.

When the NetModel query informs gmap-mod-self that the a netmodel bridge has a new netmodel port, gmap-mod-self creates a gmap port. It also links it to the gmap bridge (i.e. create `Devices.Device.<bridge>.LDevice.<port>` and `Devices.Device.<port>.UDevice.<bridge>`).

When the NetModel query informs gmap-mod-self that a netmodel port moves from a netmodel bridge to another netmodel bridge, gmap-mod-self updates gmap's model of the topology accordingly.

Remember that there was also a NetModel query that informed gmap-mod-self when there is a new netmodel bridge. This query also tells gmap-mod-self when a netmodel bridge is deleted. In that case, the gmap ports that are linked to the old gmap bridge are unlinked. The gmap ports are not deleted to support the case when they are moved to another bridge at or around the same time the old bridge is deleted.

For netmodel ports that are deleted in netmodel, the corresponding gmap port is also just unlinked from its gmap bridge.

When gmap-mod-self creates a gmap port, this new gmap device has the tags `self lan protected mac interface` and `wifi vap` if the port is a wifi vap, and `eth` if the port is an ethernet port.