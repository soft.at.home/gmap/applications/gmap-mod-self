#include "mod_sahtrace.odl";
#include "global_amxb_timeouts.odl";

%config {
    // Application name
    // this odl will be started from gmap-server
    name = "gmap-mod-self";

    // amxo parser config-options
    import-dbg = false;

    // SAHTRACE
    sahtrace = {
        type = "syslog",
        level = "${default_log_level}"
    };
    trace-zones = {
        "mod-self" = "${default_trace_zone_level}",
        "netmodel_local" = "${default_trace_zone_level}",
        "netmodel_common" = "${default_trace_zone_level}",
        "netmodel_client" = "${default_trace_zone_level}"
    };

    %global gmap-mod-self_selfdev-name = "self";

m4_ifelse(CONFIG_SAH_SERVICES_GMAP_SELF_DEVTYPE_WIFIREPEATER,y,`
    %global gmap-mod-self_devtype = "wifirepeater";
',`
    %global gmap-mod-self_devtype = "hgw";
')

    import-dirs = [
        ".",
        "${prefix}${plugin-dir}/${name}",
        "${prefix}${plugin-dir}/modules",
        "${prefix}/usr/local/lib/amx/${name}",
        "${prefix}/usr/local/lib/amx/modules",        
        "${prefix}${plugin-dir}/gmap-client/modules"
    ];

    include-dirs = [
        ".",
        "${prefix}${cfg-dir}/${name}",
        "${prefix}${cfg-dir}/modules",
        "${prefix}${cfg-dir}/gmap-client/modules"
    ];
}

import "${name}.so" as "${name}";

requires "NetModel.Intf.";
requires "Devices.Device.";
requires "DeviceInfo.";

%define {
    entry-point gmap-mod-self.gmap_self_main;
}
