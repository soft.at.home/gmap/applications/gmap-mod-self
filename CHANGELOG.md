# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v1.7.5 - 2024-12-19(08:51:42 +0000)

### Other

- [Hosts] Incorrect Layer1Interface for Wi-Fi hosts

## Release v1.7.4 - 2024-10-24(10:23:29 +0000)

### Other

- CLONE - [Devices > icon]'Unkown' is displayed for VMS device

## Release v1.7.3 - 2024-08-12(13:59:07 +0000)

### Other

- - add WDS support

## Release v1.7.2 - 2024-08-06(09:39:09 +0000)

### Fixes

- Fix on ubus-only startup sometimes postponed forever

## Release v1.7.1 - 2024-07-25(09:04:34 +0000)

### Fixes

- Fix no tracelogs + log on query success

## Release v1.7.0 - 2024-05-03(13:34:04 +0000)

### Other

- Adapt to new link API

## Release v1.6.1 - 2024-04-10(10:06:12 +0000)

### Changes

- Make amxb timeouts configurable

## Release v1.6.0 - 2024-03-26(16:24:12 +0000)

### Other

- Write documentation and draw diagrams

## Release v1.5.3 - 2024-02-21(08:45:02 +0000)

### Other

- Do not load gmap-server config since gmap-server does that by itself now

## Release v1.5.2 - 2024-02-20(11:44:39 +0000)

### Other

- Install config defaults in /etc/amx/ instead of in /etc/config/

## Release v1.5.1 - 2024-02-08(08:56:40 +0000)

### Other

- Make bridges and ports (and self) persistent

## Release v1.5.0 - 2024-01-11(09:48:41 +0000)

### Other

- Sync NetModel.Intf.i.InterfacePath => Devices.Device.i.Layer1Interface

## Release v1.4.5 - 2024-01-08(15:35:07 +0000)

### Fixes

- Fix ipv4 discoping not robust when low gmap/system performance

## Release v1.4.4 - 2023-11-15(16:56:30 +0000)

### Other

- Create wifi vaps in gmap-mod-self

## Release v1.4.3 - 2023-11-03(12:39:15 +0000)

### Other

- Fix gmap-mod-self dependencies

## Release v1.4.2 - 2023-11-02(10:49:15 +0000)

### Other

- [amx][gmap] move non-generic functions out of libgmap-client

## Release v1.4.1 - 2023-09-07(13:56:49 +0000)

### Other

- [amx][gmap] segfault of (module loaded by) gmap-client

## Release v1.4.0 - 2023-06-06(09:36:49 +0000)

### Other

- use amxut

## Release v1.3.2 - 2023-06-02(09:00:29 +0000)

### Other

- [gmap] MaxDevices implementation doesnt differentiate between devices and interfaces

## Release v1.3.1 - 2023-04-17(09:09:41 +0000)

### Other

- Removed deprecated odl keywords in tests

## Release v1.3.0 - 2023-02-24(11:42:42 +0000)

### Other

- Fix no logging when running standalone

## Release v1.2.3 - 2023-02-23(10:56:11 +0000)

### Other

- Adapt to new setActive API with source and priority

## Release v1.2.2 - 2023-02-15(08:30:38 +0000)

### Other

- Adapt to 'nemo' tag not used anymore

## Release v1.2.1 - 2023-01-10(10:01:45 +0000)

### Fixes

- Use search path in requires statement for instance object

## Release v1.2.0 - 2023-01-05(11:54:58 +0000)

### New

- [import-dbg] Disable import-dbg by default for all amxrt plugin

## Release v1.1.0 - 2022-12-01(10:20:08 +0000)

### Other

- Sync bridge IP from netmodel + add 'bridge' tag

## Release v1.0.2 - 2022-08-30(10:15:17 +0000)

### Other

- Add missing requires

## Release v1.0.1 - 2022-08-22(14:25:35 +0000)

### Other

- remove "wifirepeater" tag until clear why needed

## Release v1.0.0 - 2022-07-26(08:40:05 +0000)

### Other

- [prpl][gMap] gMap needs to be split in a gMap-core and gMap-client process

## Release v0.0.17 - 2022-07-14(09:06:45 +0000)

### Other

- Switch to using netmodel and amxs

## Release v0.0.16 - 2022-05-19(12:54:23 +0000)

### Fixes

- [Gitlab CI][Unit tests][valgrind] Pipeline doesn't stop when memory leaks are detected

## Release v0.0.15 - 2021-11-02(12:16:28 +0000)

### Changes

- remove symlink to init script

## Release v0.0.14 - 2021-10-04(10:43:21 +0000)

### Fixes

- [GMAP-MOD-SELF] add ; in odl

## Release v0.0.13 - 2021-09-29(15:30:33 +0000)

### Fixes

- [GMAP-MOD-SELF] wait for network.device on ubus

## Release v0.0.12 - 2021-07-19(14:45:51 +0000)

### Fixes

- [tr181 plugins][makefile] Dangerous clean target for all tr181 components
- relative path in odl to .so file fails

## Release 0.0.9 - 2021-05-03(10:23:15 +0000)

### Fixes

- Fix package copyright path

## Release 0.0.8 - 2021-04-13(09:30:11 +0000)

### Changes

- Modules and plugins should not have a version extension in their names

## Release 0.0.7 - 2021-04-07(12:55:05 +0000)

### Changes

- Add README and CHANGELOG

## Release 0.0.6 - 2021-03-31(15:12:00 +0100)

### Changes

- remove openwrt and yocto dirs from gmap-mod-self repo

## Release 0.0.5 - 2021-03-30(15:55:00 +0100)

### New

- Add startup script

## Release 0.0.4 - 2021-03-26(11:17:00 +0100)

### Changes

- convert gmap-self to amx

## Release 0.0.3 - 2021-03-23(14:42:00 +0100)

### Changes

- convert gmap-self to amx

## Release 0.0.2 - 2021-03-16(15:20:00 +0100)

### New

- Initial release
